"""feat: tabla nodo creada

Revision ID: 1396f8543585
Revises: 57776783afbd
Create Date: 2021-07-04 20:18:32.735801

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1396f8543585'
down_revision = '57776783afbd'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('node',
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('created_by', sa.String(), nullable=True),
    sa.Column('updated_by', sa.String(), nullable=True),
    sa.Column('status', sa.Boolean(), nullable=True),
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('id_neuralactions', sa.String(), nullable=True),
    sa.Column('node_type', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_node_id'), 'node', ['id'], unique=False)
    op.create_index(op.f('ix_node_id_neuralactions'), 'node', ['id_neuralactions'], unique=False)
    op.create_index(op.f('ix_node_node_type'), 'node', ['node_type'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_node_node_type'), table_name='node')
    op.drop_index(op.f('ix_node_id_neuralactions'), table_name='node')
    op.drop_index(op.f('ix_node_id'), table_name='node')
    op.drop_table('node')
    # ### end Alembic commands ###
