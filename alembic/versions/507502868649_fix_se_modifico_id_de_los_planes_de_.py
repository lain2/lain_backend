"""fix: se modifico id de los planes de estudio

Revision ID: 507502868649
Revises: 82745b4ad5f1
Create Date: 2021-03-01 22:54:07.566136

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '507502868649'
down_revision = '82745b4ad5f1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('study_plan', 'id',
               existing_type=sa.INTEGER(),
               type_=sa.String())
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('study_plan', 'id',
               existing_type=sa.String(),
               type_=sa.INTEGER())
    # ### end Alembic commands ###
