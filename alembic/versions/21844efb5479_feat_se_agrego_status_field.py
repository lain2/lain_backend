"""feat: se agrego status field

Revision ID: 21844efb5479
Revises: 507502868649
Create Date: 2021-03-08 16:42:47.602848

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '21844efb5479'
down_revision = '507502868649'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('study_plan', sa.Column('status', sa.Boolean(), nullable=True))
    op.add_column('token_neural', sa.Column('status', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('token_neural', 'status')
    op.drop_column('study_plan', 'status')
    # ### end Alembic commands ###
