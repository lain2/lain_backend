from __future__ import with_statement

import os

from alembic import context
from sqlalchemy import engine_from_config, pool
from logging.config import fileConfig
import sys

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# sys.path.insert(0, os.path.abspath('../app'))
sys.path = ['', '../app'] + sys.path[1:]

from app.data.db.base import Base # now it can be imported
from app.config.config import settings
target_metadata = Base.metadata

def get_url():
    print()
    server : str = settings.POSTGRES_SERVER
    user : str = settings.POSTGRES_USER
    password: str = settings.POSTGRES_PASSWORD
    db : str = settings.POSTGRES_DB
    url =  f"postgresql://{user}:{password}@{server}/{db}"
    print(url)
    return url


def run_migrations_offline():

    url = get_url()
    context.configure(
        url=url, target_metadata=target_metadata, literal_binds=True, compare_type=True
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    configuration = config.get_section(config.config_ini_section)
    configuration["sqlalchemy.url"] = get_url()
    connectable = engine_from_config(
        configuration, prefix="sqlalchemy.", poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata, compare_type=True
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()

