import json
from fastapi.testclient import TestClient
from unittest import mock
from app.core.models.study_plan import StudyPlan


@mock.patch("app.core.uses_cases.study_plan.change_status_study_plan_uc.ChangeStatusStudyPlanUC.change_status",
            autospec=True)
def test_put_change_status_study_plan_and_successfull(mock_use_case, client: TestClient) -> None:

    mock_use_case.return_value = True

    url = "/api/v1/developer/study-plan/0001"

    response = client.put(url)
    updated = response.json()

    assert response.status_code == 200
    assert  updated["id"] == "0001"
    assert  updated["updated"] == True



