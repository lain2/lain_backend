import json
from fastapi.testclient import TestClient
from unittest import mock
from app.core.models.study_plan import StudyPlan
from app.rest.schemas.study_plan import GetStudyPlanScheme


@mock.patch("app.core.uses_cases.study_plan.list_all_study_plan_uc.ListAllStudyPlanUC.get_all", autospec=True)
def test_list_all_study_plan_successfull(mock_use_case, client: TestClient) -> None:

    study_plan_1 = GetStudyPlanScheme(id ="123",
                                      code ="0001",
                                      name="Lic en sistemas",
                                      description="",
                                      status = True)
    study_plan_2 = GetStudyPlanScheme(id ="234",
                                      code ="0002",
                                      name="Lic en biologia",
                                      description="",
                                      status = True)

    list_study_plan = [study_plan_1, study_plan_2]
    mock_use_case.return_value = list_study_plan

    url = '/api/v1/developer/study-plan/'
    response = client.get(url)


    assert response.status_code == 200
    assert len(response.json()) == len(list_study_plan)
    assert response.json() == list_study_plan
