import json
from fastapi.testclient import TestClient
from unittest import mock
from app.core.models.study_plan import StudyPlan


@mock.patch("app.rest.routes.study_plan.update_study_plan.update_in_neuralactions", autospec=True)
@mock.patch("app.core.uses_cases.study_plan.update_study_plan_uc.UpdateStudyPlanUC.update", autospec=True)
def test_put_update_study_plan_and_successfull(mock_use_case, mock_neural, client: TestClient) -> None:

    study_plan = StudyPlan("0001",
                           "071/08",
                           "Lic. en sistemas",
                           "description")
    mock_neural.return_value = True
    mock_use_case.return_value = study_plan

    url = "/api/v1/developer/study-plan/"
    data = {
        "id": "0001",
        "code":"071/08",
        "name":"Lic. en sistemas",
        "description":"description",
    }
    response = client.put(url, json=data)
    updated = response.json()

    assert response.status_code == 200
    assert updated["id"] == study_plan.id
    assert updated["code"] == study_plan.code
    assert updated["name"] == study_plan.name
    assert updated["description"] == study_plan.description



