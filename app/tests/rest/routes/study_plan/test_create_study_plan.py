import json
from fastapi.testclient import TestClient
from unittest import mock
from app.core.models.study_plan import StudyPlan
from app.rest.schemas.study_plan import CreateStudyPlanScheme


@mock.patch("app.rest.routes.study_plan.create_study_plan.save_in_neuralactions", return_value='0001', autospec=True)
@mock.patch("app.core.uses_cases.study_plan.create_study_plan_uc.CreateStudyPlanUC.save",
            return_value=StudyPlan("0001", "072/08", "Lic. en sistemas","sample"), autospec=True)
def test_post_create_study_plan_and_successfull(mock_neural, mock_use_case, client: TestClient) -> None:

    url = "/api/v1/developer/study-plan/"
    data = {
        "code":"072/08",
        "name":"Lic. en sistemas",
        "description":"sample",
    }
    response = client.post(url, json=data)

    print(str(response.json()))
    assert response.status_code == 200



