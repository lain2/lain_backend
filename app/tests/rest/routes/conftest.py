import pytest
from typing import Dict, Generator
from fastapi.testclient import TestClient
from app.asgi import app

@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c
