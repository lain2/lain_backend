import json
from fastapi.testclient import TestClient
from unittest import mock
from app.core.models.user import User
from app.core.models.rol import Rol
import pytest
from app.tests.rest.utils import get_token_admin




@mock.patch("app.core.uses_cases.security.create_user_uc.CreateUserUC.save", autospec=True)
def test_post_create_user_and_successfull(mock_use_case, client: TestClient) -> None:

    rol = Rol("1", "ADMIN")
    user = User("1", "Manuel", "Perez@gmail.com", "123456", "", False, rol)
    mock_use_case.return_value = user
    headers = get_token_admin()

    url = "/api/v1/developer/user/"
    data = {
        "id": "1",
        "username": "Manuel",
        "email": "Perez@gmail.com",
        "password": "123456",
        "avatar": "",
        "google": False,
        "status": False,
        "rol": {
            "id": "1",
            "name": "ADMIN"
        },
    }
    print(headers)
    response = client.post(url, json=data, headers=headers)

    print(str(response.json()))
    assert response.status_code == 200
