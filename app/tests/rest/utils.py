import os
import pytest
from typing import Any, Generator, List


from app.config.security import verify_password
from app.core.models.user import User
from app.data.crud.crud_user import crud_user
from app.data.crud.crud_rol import crud_rol
from app.data.entities.user_entity import UserEntity
from app.data.entities.rol_entity import  RolEntity
from sqlalchemy.orm import Session
from fastapi.testclient import TestClient
from app.config.config import settings
from app.tests.data.conftest import db


def clean_db(db: Session) -> None:
    db.query(UserEntity).delete()
    db.query(RolEntity).delete()
    db.commit()

def data_db(db: Session) -> List[RolEntity]:
    rol_1 = RolEntity(id="1", name="ADMIN")
    rol_2 = RolEntity(id="2", name="DOCENTE")

    roles = [rol_1, rol_2]

    db.add(rol_1)
    db.add(rol_2)
    db.commit()
    return [rol_1, rol_2]

def create_user() -> None:
    clean_db(db)
    data_db(db)
    rol =  crud_rol.get(db, "1")
    user = UserEntity(
        id="1",
        username="himura",
        email="himurakenji@gmail.com",
        password="123456",
        google=True,
        avatar='',
        rol_id= rol.id,
    )
    item = crud_user.create(db=db,user=user)



# def get_token():

#     r = Client.post(
#         f"{settings.API_V1_STR}/auth/token", headers=get_superuser_token_headers(Client),
#     )
#     result = r.json()


def get_token_admin():
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYxMzU3MTYsInVzZXIiOnsiaWQiOiIxIiwidXNlcm5hbWUiOiJyb2xvbjEyIiwiZW1haWwiOiJyb2xvbjEyQGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiJDJiJDEyJFEyeG1XQU01ZXZMMUZRb28ybXAxWk9FaXBaSzYweElLL1c4YkE0SFc1TnEuYzFWSGlWVEVpIiwiYXZhdGFyIjoiIiwiZ29vZ2xlIjp0cnVlLCJzdGF0dXMiOnRydWUsInJvbCI6eyJpZCI6IjEiLCJuYW1lIjoiQURNSU4iLCJzdGF0dXMiOnRydWV9fX0.LcQQKZxkC0iH271wRiU_IUpy4hRFqjsf2dBaz6w-0C4'
    headers = {"Authorization": f"Bearer {token}"}
    return headers
