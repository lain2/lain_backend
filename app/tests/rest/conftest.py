import pytest
from typing import Dict, Generator
from fastapi.testclient import TestClient
from app.data.db.session import SessionLocal
from sqlalchemy.orm import Session
from app.asgi import app

@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="session")
def db() -> Session:
    yield SessionLocal()
