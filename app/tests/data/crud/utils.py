import pytest
from sqlalchemy.orm import Session

from app.data.entities.rol_entity import RolEntity
from app.data.entities.user_entity import UserEntity
from app.data.entities.person_entity import PersonEntity

@pytest.fixture(scope="function")
def clean_db(db: Session) -> None:
    db.query(PersonEntity).delete()
    db.query(UserEntity).delete()
    db.query(RolEntity).delete()
    db.commit()
