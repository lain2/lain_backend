import os
import pytest
from typing import Any, Generator, List

from app.core.models.study_plan import StudyPlan
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.entities.study_plan_entity import StudyPlanEntity
from sqlalchemy.orm import Session



@pytest.fixture(scope = 'session')
def clean_db(db: Session) -> None:
    db.query(StudyPlanEntity).delete()
    db.commit()

@pytest.fixture(scope = 'session')
def list_study_plans(db: Session) -> List[StudyPlanEntity]:

    study_plan_1 = StudyPlanEntity(id='123',
                                   code='0001',
                                   name='Lic. en sistemas',
                                   description='Sample')

    study_plan_2 = StudyPlanEntity(id='234',
                                   code='0002',
                                   name='Lic. en mecatronica',
                                   description='Sample')

    db.add(study_plan_1)
    db.add(study_plan_2)

    db.commit()

    return [study_plan_1, study_plan_2]


def test_list_all_study_plans(db: Session, clean_db, list_study_plans) -> None:
    study_plans = crud_study_plan.get_all(db=db)

    assert len(study_plans) == len(list_study_plans)
    assert study_plans == list_study_plans


