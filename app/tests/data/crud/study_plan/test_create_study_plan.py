import os
import pytest
from typing import Any, Generator

from app.core.models.study_plan import StudyPlan
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.entities.study_plan_entity import StudyPlanEntity
from sqlalchemy.orm import Session


@pytest.fixture(scope='session')
def clean_db(db: Session) -> None:
    db.query(StudyPlanEntity).delete()
    db.commit()


def test_create_item(db: Session, clean_db) -> None:
    study_plan = StudyPlan("0001", "123", "071/08", "ing en sistemas")
    item = crud_study_plan.create(db=db, study_plan=study_plan)

    assert item.id == "0001"
    assert item.code == "123"
    assert item.name == "071/08"
    assert item.status == True
    assert item.description == "ing en sistemas"

