import os
import pytest
from typing import Any, Generator, List

from app.core.models.study_plan import StudyPlan
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.entities.study_plan_entity import StudyPlanEntity
from sqlalchemy.orm import Session


@pytest.fixture(scope='function')
def clean_db(db: Session) -> None:
    db.query(StudyPlanEntity).delete()
    db.commit()

@pytest.fixture(scope = 'function')
def data_db(db: Session) -> List[StudyPlanEntity]:

    study_plan = StudyPlanEntity(id='123',
                                   code='0001',
                                   name='Lic. en alimentos',
                                   description='Sample',
                                   status = True)

    db.add(study_plan)
    db.commit()

    return study_plan


def test_update_status_item(db: Session, clean_db, data_db: StudyPlanEntity) -> None:

    id = "123"
    study_plan = StudyPlan("123",
                           "071/08",
                           "Lic. en sistemas",
                           "Ejemplo")
    study_plan.status = False
    item = crud_study_plan.update_status(db=db, study_plan = study_plan )

    assert item.id == "123"
    assert item.code == "0001"
    assert item.status == False


def test_get_study_plan_by_code(db: Session, clean_db, data_db: List[StudyPlanEntity]) -> None:

    item = crud_study_plan.get(db=db, id = "123")

    assert item.code == "0001"
    assert item.name == "Lic. en alimentos"

