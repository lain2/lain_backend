import os
import pytest
from typing import Any, Generator, List

from app.core.models.study_plan import StudyPlan
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.entities.study_plan_entity import StudyPlanEntity
from sqlalchemy.orm import Session


@pytest.fixture(scope='function')
def clean_db(db: Session) -> None:
    db.query(StudyPlanEntity).delete()
    db.commit()

@pytest.fixture(scope = 'function')
def data_db(db: Session) -> List[StudyPlanEntity]:

    study_plan_1 = StudyPlanEntity(id='123',
                                   code='0001',
                                   name='Lic. en alimentos',
                                   description='Sample',
                                   status = True)

    study_plan_2 = StudyPlanEntity(id='234',
                                   code='0002',
                                   name='Ing. en sistemas',
                                   description='Sample')

    db.add(study_plan_1)
    db.add(study_plan_2)
    db.commit()

    return [study_plan_1, study_plan_2]


def test_update_item(db: Session, clean_db, data_db: List[StudyPlanEntity]) -> None:

    study_plan = StudyPlan("123",
                           "071/08",
                           "Lic. en sistemas",
                           "Ejemplo")

    item = crud_study_plan.update(db=db, study_plan=study_plan)

    assert item.id == "123"
    assert item.code == "071/08"
    assert item.name == "Lic. en sistemas"
    assert item.description == "Ejemplo"
    assert item.status == True



def test_get_study_plan_by_code(db: Session, clean_db, data_db: List[StudyPlanEntity]) -> None:

    item = crud_study_plan.get_by_code(db=db, code="0001")

    assert item.code == "0001"
    assert item.name == "Lic. en alimentos"

