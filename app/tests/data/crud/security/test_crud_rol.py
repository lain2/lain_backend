import os
import pytest
from typing import Any, Generator, List

from app.data.crud.crud_rol import crud_rol
from app.data.entities.rol_entity import RolEntity
from app.data.entities.user_entity import UserEntity
from sqlalchemy.orm import Session
from app.tests.data.crud.utils import clean_db


# @pytest.fixture(scope='function')
# def clean_db(db: Session) -> None:
#     db.query(UserEntity).delete()
#     db.query(RolEntity).delete()
#     db.commit()

@pytest.fixture(scope = 'function')
def data_db(db: Session) -> List[RolEntity]:

    rol_1 = RolEntity(id="1", name="ADMIN")
    rol_2 = RolEntity(id="2", name="DOCENTE")
    roles = [rol_1, rol_2]

    db.add(rol_1)
    db.add(rol_2)
    db.commit()

    return [rol_1, rol_2]


def test_create_rol(db: Session, clean_db) -> None:

    rol_entity = RolEntity(id="123", name="ADMIN")
    item = crud_rol.create(db=db, obj_in=rol_entity)

    assert item.id == "123"
    assert item.name == "ADMIN"


def test_update_rol(db: Session, clean_db, data_db: List[RolEntity]) -> None:

    rol_entity = RolEntity(id="2", name="PROFESOR")
    item = crud_rol.update(db=db, id="2", obj_in=rol_entity)

    assert item.id == "2"
    assert item.name == "PROFESOR"


def test_get_rol_by_name(db: Session, clean_db, data_db: List[RolEntity]) -> None:

    item = crud_rol.get_by_name(db=db, name="ADMIN")

    assert item.id == "1"
    assert item.name == "ADMIN"
