import os
import pytest
from typing import Any, Generator, List

from app.config.security import verify_password
from app.core.models.user import User
from app.data.crud.crud_user import crud_user
from app.data.crud.crud_rol import crud_rol
from app.data.entities.user_entity import UserEntity
from app.data.entities.rol_entity import  RolEntity
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder


@pytest.fixture(scope='function')
def clean_db(db: Session) -> None:
    db.query(UserEntity).delete()
    db.query(RolEntity).delete()
    db.commit()

@pytest.fixture(scope = 'function')
def data_db(db: Session) -> List[RolEntity]:
    rol_1 = RolEntity(id="1", name="ADMIN")
    rol_2 = RolEntity(id="2", name="DOCENTE")

    roles = [rol_1, rol_2]

    db.add(rol_1)
    db.add(rol_2)
    db.commit()
    return [rol_1, rol_2]

def test_create_user(db: Session, clean_db, data_db) -> None:

    print('test_create_user')

    rol =  crud_rol.get(db, "1")
    print(jsonable_encoder(rol))

    user = UserEntity(
        id="1",
        username="rolon12",
        email="rolon12@gmail.com",
        password="123",
        google=True,
        avatar='',
        rol_id= rol.id,
    )
    item = crud_user.create(db=db,user=user)

    validPassword = verify_password("123", item.password)

    assert item.id == "1"
    assert validPassword == True
    assert item.username == "rolon12"
    assert user.rol_id == "1"
    assert user.avatar == ""


