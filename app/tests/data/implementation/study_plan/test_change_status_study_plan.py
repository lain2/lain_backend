import uuid
import pytest
from sqlalchemy.orm import Session
from unittest import mock

from app.core.models.study_plan import StudyPlan
from app.data.entities.study_plan_entity import StudyPlanEntity

# Change
from app.data.implementation.study_plan.change_status_study_plan import \
    ChangeStatusStudyPlanImplementation


@mock.patch(
    "app.data.crud.crud_study_plan.crud_study_plan.update_status",
    autospec=True,
)
def test_update_status_study_plan_successfull(mock_crud) -> None:

    study_plan_core = StudyPlan("0001", "123", "Lic. en sistemas","")
    study_plan_core.status = False
    study_plan = StudyPlanEntity(id = "0001",
                                 code = "123",
                                 name = "Lic. en sistemas",
                                 description = "",
                                 status = False)

    mock_crud.return_value = study_plan

    result = ChangeStatusStudyPlanImplementation().update_status(study_plan_core)

    assert result is True


@mock.patch(
    "app.data.crud.crud_study_plan.crud_study_plan.get",
    autospec=True,
)
def test_get_by_id_status_study_plan_successfull(mock_crud) -> None:

    study_plan_core = StudyPlan("0001", "123", "Lic. en sistemas","")
    study_plan = StudyPlanEntity(id = "0001",
                                 code = "123",
                                 name = "Lic. en sistemas",
                                 description = "")
    study_plan.status = not study_plan.status

    mock_crud.return_value = study_plan

    result = ChangeStatusStudyPlanImplementation().get_by_id("0001")

    assert result == study_plan_core
