import uuid
from unittest import mock

import pytest
from app.core.models.study_plan import StudyPlan
from app.data.entities.study_plan_entity import StudyPlanEntity
from app.data.implementation.study_plan.list_all_study_plan_impl import \
    ListStudyPlanImplementation
from sqlalchemy.orm import Session


@mock.patch(
    "app.data.crud.crud_study_plan.crud_study_plan.get_all",
    autospec=True,
)
def test_list_all_study_plan_and_return_models(mock_crud) -> None:

    study_plan_1 = StudyPlanEntity(id="123",
                                   code="0001",
                                   name="Lic en sistemas",
                                   description="")
    study_plan_2 = StudyPlanEntity(id="123",
                                   code="0002",
                                   name="Lic en biologia",
                                   description="")

    mock_crud.return_value = [study_plan_1, study_plan_2]

    result = ListStudyPlanImplementation().get_all()

    assert len(result) == 2
