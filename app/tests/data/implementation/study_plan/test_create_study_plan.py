import uuid
from unittest import mock

import pytest
from app.core.models.study_plan import StudyPlan
from app.data.entities.study_plan_entity import StudyPlanEntity
from app.data.implementation.study_plan.create_study_plan import \
    CreateStudyPlanImplementation
from sqlalchemy.orm import Session


@mock.patch(
    "app.data.crud.crud_study_plan.crud_study_plan.create",
    return_value=StudyPlan(123, "123", "072/08", "sample"),
    autospec=True,
)
def test_create_study_plan(mock_crud) -> None:

    study_plan = StudyPlan(None, "123", "072/08", "sample")

    result = CreateStudyPlanImplementation().save(study_plan)

    assert study_plan.code == result.code
    assert study_plan.name == result.name
    assert study_plan.description == result.description
