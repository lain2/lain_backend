import uuid
import pytest
from sqlalchemy.orm import Session
from unittest import mock

from app.core.models.study_plan import StudyPlan
from app.data.entities.study_plan_entity import StudyPlanEntity
from app.data.implementation.study_plan.update_study_plan import \
    UpdateStudyPlanImplementation


@mock.patch(
    "app.data.crud.crud_study_plan.crud_study_plan.update",
    autospec=True,
)
def test_update_study_plan(mock_crud) -> None:

    study_plan = StudyPlan(None, "123", "072/08", "sample")
    mock_crud.return_value = study_plan

    result = UpdateStudyPlanImplementation().update(study_plan)

    assert study_plan.code == result.code
    assert study_plan.name == result.name
    assert study_plan.description == result.description

@mock.patch(
    "app.data.crud.crud_study_plan.crud_study_plan.get_by_code",
    autospec=True,
)
def test_update_study_plan(mock_crud) -> None:

    study_plan = StudyPlan(None, "123", "072/08", "sample")
    mock_crud.return_value = study_plan

    result = UpdateStudyPlanImplementation().get_by_code("123")

    assert study_plan.code == result.code
    assert study_plan.name == result.name
    assert study_plan.description == result.description
