import uuid
from unittest import mock

import pytest
from app.core.models.rol import Rol
from app.core.models.user import User
from app.data.crud.crud_user import crud_user
from app.data.entities.user_entity import UserEntity
from app.data.entities.rol_entity import RolEntity
from app.data.implementation.security.create_user import CreateUserImplementation
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder




@mock.patch(
    "app.data.crud.crud_user.crud_user.create",
    autospec=True,
)
def test_create_study_plan(mock_crud) -> None:

    rol_entity = RolEntity(id="1", name="ADMIN")
    user_entity = UserEntity(
        id="1",
        username="rolon12",
        email="rolon12@gmail.com",
        password="wfasdwasd",
        google=True,
        avatar='',
        rol_id= rol_entity.id,
    )
    mock_crud.return_value = user_entity

    rol = Rol("1", "ADMIN")
    user = User("1", "rolon12", "rolon12@gmail.com", "123", "", True, rol)

    result = CreateUserImplementation().save(user)
    assert user.id == result.id
    assert user.username== result.username
    assert user.password != result.password
    assert user.rol.id == result.rol.id
    assert user.rol.name == result.rol.name


@mock.patch(
    "app.data.crud.crud_user.crud_user.get_by_email",
    autospec=True,
)
def test_get_user_by_email(mock_crud) -> None:
    rol_entity = RolEntity(id="1", name="ADMIN")
    user_entity = UserEntity(
        id="1",
        username="rolon12",
        email="rolon12@gmail.com",
        password="wfasdwasd",
        google=True,
        avatar='',
        rol_id= rol_entity.id,
    )
    mock_crud.return_value = user_entity
    rol = Rol("1", "ADMIN")
    user = User("1", "rolon12", "rolon12@gmail.com", "123", "", True, rol)

    result = CreateUserImplementation().get_by_email(email="rolon12@gmail.com")

    assert user.id == result.id
    assert user.username == result.username
    assert user.password != result.password
    assert user.rol.id == result.rol.id
    assert user.rol.name == result.rol.name
