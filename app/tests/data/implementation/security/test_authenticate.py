import uuid
from unittest import mock

import pytest
from app.core.models.rol import Rol
from app.core.models.user import User
from app.data.crud.crud_user import crud_user
from app.data.entities.user_entity import UserEntity
from app.data.entities.rol_entity import RolEntity
from app.data.implementation.security.authenticate import AuthenticateImplementation
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from loguru import logger

@pytest.fixture(scope="module")
def data():
    rol_entity = RolEntity(id="1", name="ADMIN")
    user_entity = UserEntity(
        id="1",
        username="himura",
        email="himurakenji@gmail.com",
        password="kenji",
        google=True,
        avatar='',
        rol_id= rol_entity.id,
    )
    return user_entity



@mock.patch(
    "app.data.crud.crud_user.crud_user.get_by_username",
    autospec=True,
)
def test_get_user_by_username_implementation(mock_crud, data) -> None:

    mock_crud.return_value = data

    result = AuthenticateImplementation().get_user_by_username("himura")


    assert data.id == result.id
    assert data.username== result.username
    assert data.password == result.password


