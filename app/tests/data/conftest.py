from typing import Generator

import pytest
from app.data.db.session import SessionLocal
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


@pytest.fixture(scope="session")
def db() -> Session:
    yield SessionLocal()
