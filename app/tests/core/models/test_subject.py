import pytest
import uuid

from app.core.models.subject import Subject
from app.core.models.study_plan import StudyPlan


@pytest.fixture(scope="function")
def study_plan():
    id = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    return StudyPlan(id, code, name, description, True)


def test_subject_init(study_plan):
    id = uuid.uuid4()
    code = '001'
    name = 'Programacion I'
    regime = 'anual'
    hours_per_week = 10
    total_hours = 120

    subject = Subject(id, name, code, regime, hours_per_week, total_hours, True, study_plan)

    assert subject.id == id
    assert subject.name == name
    assert subject.code == code
    assert subject.regime == regime
    assert subject.hours_per_week == hours_per_week
    assert subject.total_hours == total_hours
    assert subject.status == True
    assert subject.study_plan == study_plan


def test_subject_equals_code(study_plan):
    id1 = uuid.uuid4()
    id2  = uuid.uuid4()
    name = 'Programacion I'
    regime = 'anual'
    hours_per_week = 10
    total_hours = 120

    subject = Subject(id1, name, '001', regime, hours_per_week, total_hours, True, study_plan)
    subject2 = Subject(id2, name, '001', regime, hours_per_week, total_hours, True, study_plan)

    assert subject == subject2

def test_subject_not_equals(study_plan):
    id1 = uuid.uuid4()
    id2  = uuid.uuid4()
    name = 'Programacion I'
    regime = 'anual'
    hours_per_week = 10
    total_hours = 120

    subject = Subject(id1, name, '001', regime, hours_per_week, total_hours, True, study_plan)
    subject2 = Subject(id2, name, '002', regime, hours_per_week, total_hours, True, study_plan)

    assert subject != subject2

