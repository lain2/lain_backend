import pytest
import uuid

from app.core.models.study_plan import StudyPlan
from app.core.models.subject import Subject
from app.core.models.unit import Unit



@pytest.fixture(scope="function")
def study_plan():
    id = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    return StudyPlan(id, code, name, description, True)


@pytest.fixture(scope="function")
def subject(study_plan):
    id = uuid.uuid4()
    code = '001'
    name = 'Programacion I'
    regime = 'anual'
    hours_per_week = 10
    total_hours = 120

    subject = Subject(id, name, code, regime, hours_per_week, total_hours, True, study_plan)


def test_unit_init(subject):
    id = uuid.uuid4()
    number = 1
    name =  'Punteros'
    description = 'Se trata el tema de cuando un se hace referencia a ...'
    objectives = 'Que el alumno aprenda'

    unit = Unit(id, number, name, description, objectives, True, subject)

    assert  unit.id == id
    assert  unit.number == number
    assert  unit.name == name
    assert  unit.description == description
    assert  unit.objectives == objectives
    assert  unit.status == True
    assert  unit.subject == subject



def test_unit_not_equals(subject):
    id1 = uuid.uuid4()
    id2 = uuid.uuid4()
    number = 1
    name1 =  'Punteros1'
    name2 =  'Punteros2'
    description = 'Se trata el tema de cuando un se hace referencia a ...'
    objectives = 'Que el alumno aprenda'

    unit1 = Unit(id1, number, name1, description, objectives, True, subject)
    unit2 = Unit(id2, number, name2, description, objectives, True, subject)

    assert unit1 != unit2


def test_unit_same_name(subject):
    id1 = uuid.uuid4()
    id2 = uuid.uuid4()
    number = 1
    name =  'Punteros'
    description = 'Se trata el tema de cuando un se hace referencia a ...'
    objectives = 'Que el alumno aprenda'

    unit1 = Unit(id1, number, name, description, objectives, True, subject)
    unit2 = Unit(id2, number, name, description, objectives, True, subject)

    assert unit1 == unit2

