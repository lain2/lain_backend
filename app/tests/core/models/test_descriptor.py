import pytest
import uuid

from app.core.models.bibliography import Bibliography
from app.core.models.descriptor import Descriptor


@pytest.fixture(scope='module')
def bibliography():

    bibliography1 = Bibliography(uuid.uuid4(), 'Chainsaw Man', '1974709930', 'data//chainsaw.jpeg', True)
    bibliography2 = Bibliography(uuid.uuid4(), 'Naruto', '1974709931', 'data//naruto.jpeg', True)
    bibliography3 = Bibliography(uuid.uuid4(), 'Rurouni Kenji', '1974709932', 'data//kenji.jpeg', True)
    bibliography4 = Bibliography(uuid.uuid4(), 'Mob psyco', '1974709933', 'data//mob.jpeg', True)

    return [bibliography1, bibliography2, bibliography3, bibliography4]


def test_descriptor_init():

    id = uuid.uuid4()
    name = 'Data structure'
    description = 'The data structure is so important in computer science.'

    descriptor = Descriptor(id, name, description, True)

    assert descriptor.id == id
    assert descriptor.name == name
    assert descriptor.description == description
    assert descriptor.status == True


def test_set_bibliography(bibliography):

    id = uuid.uuid4()
    name = 'Data structure'
    description = 'The data structure is so important in computer science.'

    descriptor = Descriptor(id, name, description, True)
    descriptor.bibliography = bibliography

    assert len(descriptor.bibliography) == len(bibliography)
