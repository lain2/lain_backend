import pytest
import uuid

from app.core.models.rol import Rol

def test_roles_init():
    id = uuid.uuid4()
    name = "ADMIN"
    rol = Rol(id, name, True)

    assert rol.id == id
    assert rol.name == name
    assert rol.status == True


def test_roles_init_status_false():
    id = uuid.uuid4()
    name = "ADMIN"
    rol = Rol(id, name, False)

    assert rol.id == id
    assert rol.name == name
    assert rol.status == False


def test_roles_equals():
    id = uuid.uuid4()
    name = "ADMIN"
    rol = Rol(id, name, False)
    rol2 = Rol(id, name, False)

    assert rol == rol2

