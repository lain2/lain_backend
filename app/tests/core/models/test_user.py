import pytest
import uuid

from app.core.models.rol import Rol
from app.core.models.user import User


@pytest.fixture(scope="function")
def rol():
    return Rol(uuid.uuid4(), "ADMIN", True)


def test_user_init(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id, username, email, password, avatar, google, status, rol)

    assert user.id == id
    assert user.username == username
    assert user.email == email
    assert user.rol == rol


def test_user_equals(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id, username, email, password, avatar, google, status, rol)
    user2 = User(id, username, email, password, avatar, google, status, rol)

    assert user == user2


def test_user_not_equals(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id, username, email, password, avatar, google, status, rol)
    user2 = User(uuid.uuid4(), 'himuraxkenji2', 'himura@mail.com', password, avatar, google, status, rol)

    assert user != user2


def test_user_same_email(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id, username, email, password, avatar, google, status, rol)
    user2 = User(uuid.uuid4(), 'himuraxkenji2', email, password, avatar, google, status, rol)

    assert user == user2
