import pytest
import uuid

from app.core.models.study_plan import StudyPlan
from app.core.models.subject import Subject
from app.core.models.program import Program



@pytest.fixture(scope="function")
def study_plan():
    id = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    return StudyPlan(id, code, name, description, True)


@pytest.fixture(scope="function")
def subject(study_plan):
    id = uuid.uuid4()
    code = '001'
    name = 'Programacion I'
    regime = 'anual'
    hours_per_week = 10
    total_hours = 120

    subject = Subject(id, name, code, regime, hours_per_week, total_hours, True, study_plan)


def test_program_init(subject):
    id = uuid.uuid4()
    minimal_content = 'This is a test of minimal content'
    fundaments = 'Fundaments for a test'
    objectives = 'Objectives for a test'
    methodology = 'Methodology for a test'
    evaluation = 'Evaluation for a test'
    general_bibliography = 'Bibiliography for a test'

    program = Program(id, minimal_content, fundaments, objectives, methodology, evaluation, general_bibliography, True, subject)

    assert  program.id == id
    assert  program.minimal_content == minimal_content
    assert  program.fundaments == fundaments
    assert  program.objectives == objectives
    assert  program.methodology == methodology
    assert  program.evaluation == evaluation
    assert  program.general_bibliography == general_bibliography
    assert  program.status == True
    assert  program.subject == subject



def test_programs_equals(subject):
    id = uuid.uuid4()
    minimal_content = 'This is a test of minimal content'
    fundaments = 'Fundaments for a test'
    objectives = 'Objectives for a test'
    methodology = 'Methodology for a test'
    evaluation = 'Evaluation for a test'
    general_bibliography = 'Bibiliography for a test'

    program1 = Program(id, minimal_content, fundaments, objectives, methodology, evaluation, general_bibliography, True, subject)
    program2 = Program(id, minimal_content, fundaments, objectives, methodology, evaluation, general_bibliography, True, subject)

    assert program1 == program2

