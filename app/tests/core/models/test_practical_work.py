import pytest
import uuid

from app.core.models.study_plan import StudyPlan
from app.core.models.subject import Subject
from app.core.models.practical_work import PracticalWork



@pytest.fixture(scope="function")
def study_plan():
    id = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    return StudyPlan(id, code, name, description, True)


@pytest.fixture(scope="function")
def subject(study_plan):
    id = uuid.uuid4()
    code = '001'
    name = 'Programacion I'
    regime = 'anual'
    hours_per_week = 10
    total_hours = 120

    subject = Subject(id, name, code, regime, hours_per_week, total_hours, True, study_plan)


def test_practical_work_init(subject):
    id = uuid.uuid4()
    name = 'Punteros a funcion'
    number = 1
    activity = 'Realizar un puntero'
    materials = 'Computadora y editor de codigo'
    practice_enviroment = 'Laboratorio'

    practical_work = PracticalWork(id, name, number, activity, materials, practice_enviroment, True, subject)


    assert  practical_work.id == id
    assert  practical_work.name == name
    assert  practical_work.number == number
    assert  practical_work.activity == activity
    assert  practical_work.materials == materials
    assert  practical_work.practice_enviroment == practice_enviroment
    assert  practical_work.status == True
    assert  practical_work.subject == subject



def test_practical_work_equals_by_name(subject):
    id1 = uuid.uuid4()
    id2 = uuid.uuid4()
    name = 'Punteros a funcion'
    number = 1
    activity = 'Realizar un puntero'
    materials = 'Computadora y editor de codigo'
    practice_enviroment = 'Laboratorio'

    practical_work1 = PracticalWork(id1, name, number, activity, materials, practice_enviroment, True, subject)
    practical_work2 = PracticalWork(id2, name, number, activity, materials, practice_enviroment, True, subject)

    assert practical_work1 == practical_work2


def test_practical_work_not_equals(subject):
    id1 = uuid.uuid4()
    id2 = uuid.uuid4()
    name1 = 'Punteros a funcion'
    name2 = 'Punteros a funciones'
    number = 1
    activity = 'Realizar un puntero'
    materials = 'Computadora y editor de codigo'
    practice_enviroment = 'Laboratorio'

    practical_work1 = PracticalWork(id1, name1, number, activity, materials, practice_enviroment, True, subject)
    practical_work2 = PracticalWork(id2, name2, number, activity, materials, practice_enviroment, True, subject)

    assert practical_work1 != practical_work2


