import pytest
import uuid

from app.core.models.study_plan import StudyPlan


def test_study_plan_init():
    id = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    study_plan = StudyPlan(id, code, name, description, True)

    assert study_plan.id == id
    assert study_plan.code == code
    assert study_plan.name == name
    assert study_plan.description == description
    assert study_plan.status == True


def test_persona_equals_code():
    id = uuid.uuid4()
    id2 = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    study_plan = StudyPlan(id, code, name, description, True)
    study_plan2 = StudyPlan(id2, code, name, description, True)

    assert study_plan == study_plan2


def test_persona_not_equals_code():
    id = uuid.uuid4()
    id2 = uuid.uuid4()
    code = '071/08'
    name = 'Lic. en sistemas'
    description = 'Plan de estudio generado para la carrera de grado'

    study_plan = StudyPlan(id, '071/08', name, description, True)
    study_plan2 = StudyPlan(id2, '071/09', name, description, True)

    assert study_plan != study_plan2

