import pytest
import uuid

from app.core.models.person import Person
from app.core.models.rol import Rol
from app.core.models.user import User


@pytest.fixture(scope='module')
def user():
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False
    rol = Rol(uuid.uuid4(), 'ADMIN', True)

    return User(id, username, email, password, avatar, google, status, rol)



def test_person_init(user):
    id = uuid.uuid4()
    lastname = 'Uchiha'
    firstname = 'Itachi'
    position = 'owner'
    avatar = 'https://p4.wallpaperbetter.com/wallpaper/381/33/164/naruto-shippuuden-realistic-sakimichan-uchiha-itachi-wallpaper-preview.jpg'

    person = Person(id, lastname, firstname, position, avatar, True, user)

    assert person.id == id
    assert person.lastname == lastname
    assert person.firstname == firstname
    assert person.position == position
    assert person.avatar == avatar


def test_persona_equals():
    id = uuid.uuid4()
    lastname = 'Uchiha'
    firstname = 'Itachi'
    position = 'owner'
    avatar = 'https://p4.wallpaperbetter.com/wallpaper/381/33/164/naruto-shippuuden-realistic-sakimichan-uchiha-itachi-wallpaper-preview.jpg'

    person = Person(id, lastname, firstname, position, avatar, True, user)
    person2 = Person(id, lastname, firstname, position, avatar, True, user)

    assert person == person2

