import pytest
import uuid

from app.core.models.bibliography import Bibliography


def test_bibliography_init():
    id = uuid.uuid4()
    name = 'Chainsaw Man'
    isbn = '1974709930'
    picture = 'https://www.normaeditorial.com/upload/media/albumes/0001/07/thumb_6655_albumes_big.jpeg'

    bibliography = Bibliography(id, name, isbn, picture, True)

    assert bibliography.id == id
    assert bibliography.name == name
    assert bibliography.isbn == isbn
    assert bibliography.picture == picture
    assert bibliography.status == True


def test_bibliography_equals():
    id = uuid.uuid4()
    name = 'Chainsaw Man'
    isbn = '1974709930'
    picture = 'https://www.normaeditorial.com/upload/media/albumes/0001/07/thumb_6655_albumes_big.jpeg'

    bibliography = Bibliography(id, name, isbn, picture, True)
    bibliography2 = Bibliography(uuid.uuid4(), name, isbn, picture, True)

    assert bibliography == bibliography2


def test_bibliography_not_equals():
    id = uuid.uuid4()
    name = 'Chainsaw Man'
    isbn = '1974709930'
    picture = 'https://www.normaeditorial.com/upload/media/albumes/0001/07/thumb_6655_albumes_big.jpeg'

    bibliography = Bibliography(id, name, isbn, picture, True)
    bibliography2 = Bibliography(uuid.uuid4(), name, '123123123', picture, True)

    assert bibliography != bibliography2

