import uuid
import pytest
from unittest import mock

from app.core.models.rol import Rol
from app.core.models.user import User
from app.core.models.person import Person
from app.core.exceptions.person_exception import PersonException
from app.core.uses_cases.security.get_all_persons_uc import GetAllPersonsUC


@pytest.fixture
def rols():

    admin = Rol('001', 'ADMIN', True)
    teacher = Rol('002', 'PROFESOR', True)

    return [admin, teacher]


@pytest.fixture
def users(rols):
    user1 = User('001', 'saitama', 'saitama@saitama', '123456', 'https://i.pinimg.com/originals/51/3b/b7/513bb7305275cb7abba98af4b0aeb88e.jpg', False, True, rols[0])
    user2 = User('002', 'tatsumaki', 'tatsumaki@tatsumaki', '123456', 'https://i.pinimg.com/originals/18/2d/fa/182dfa3a6e441f8ce2717ecb2c946b0d.jpg', False, True, rols[1])
    return [user1, user2]


@pytest.fixture
def persons(users):
    avatar = 'https://i.pinimg.com/originals/3d/cb/ac/3dcbac445e57977db9c548265af1be22.jpg'

    person1 = Person('001', 'Kenshin', 'Himura', 'Hero', 'https://upload.wikimedia.org/wikipedia/en/thumb/7/7d/Tomo1-1.jpg/200px-Tomo1-1.jpg', True, users[0])
    person2 = Person('002', 'Makoto', 'Shishio', 'Villain', 'https://upload.wikimedia.org/wikipedia/en/b/b3/ShishioMakoto.png', True, users[0])
    person3 = Person('003', 'Kaoru', 'Kamiya', 'Hero', 'https://upload.wikimedia.org/wikipedia/en/thumb/c/c6/Kaorukanzenban2.jpg/230px-Kaorukanzenban2.jpg', False, users[1])
    person4 = Person('004', 'Kenji', 'Himura', 'Hero', 'https://upload.wikimedia.org/wikipedia/en/thumb/c/c6/Kaorukanzenban2.jpg/230px-Kaorukanzenban2.jpg', True, users[0])

    return [person1, person2, person3, person4]


def test_when_get_all_persons_with_no_filters_successfull(persons):


    repo = mock.Mock()
    repo.get_all.return_value = persons

    uc_person = GetAllPersonsUC(repo)
    all_persons = uc_person.get_all()

    assert len(all_persons) == len(persons)


def test_when_get_all_persons_by_username_or_email_contains(persons):

    persons_username = [persons[0], persons[1], persons[3]]

    repo = mock.Mock()
    repo.get_all_by_username.return_value = persons_username

    uc_person = GetAllPersonsUC(repo)
    all_persons = uc_person.get_all(username = 'saitama')


    assert len(all_persons) == len(persons_username)


def test_when_get_all_persons_status_active(persons):

    persons_active = [persons[0], persons[1], persons[3]]

    repo = mock.Mock()
    repo.get_all_by_status.return_value = persons_active

    uc_person = GetAllPersonsUC(repo)
    all_persons = uc_person.get_all(status = True)


    assert len(all_persons) == len(persons_active)


def test_when_get_all_persons_status_deactive(persons):

    persons_deactive = [persons[2]]

    repo = mock.Mock()
    repo.get_all_by_status.return_value = persons_deactive

    uc_person = GetAllPersonsUC(repo)
    all_persons = uc_person.get_all(status = False)


    assert len(all_persons) == len(persons_deactive)
    assert persons_deactive[0].status == False



def test_when_get_all_persons_by_rol(persons):

    persons_by_rol = [persons[2]]

    repo = mock.Mock()
    repo.get_all_by_rol.return_value = persons_by_rol

    uc_person = GetAllPersonsUC(repo)
    all_persons = uc_person.get_all(rol = 'PROFESOR')

    assert len(all_persons) == len(persons_by_rol)
    assert persons_by_rol[0].user.rol.name == 'PROFESOR'
