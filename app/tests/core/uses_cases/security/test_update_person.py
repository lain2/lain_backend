import uuid
import pytest
from unittest import mock

from app.core.models.rol import Rol
from app.core.models.user import User
from app.core.models.person import Person
from app.core.exceptions.person_exception import PersonException
from app.core.repositories.security.repo_update_person import RepoUpdatePerson
from app.core.uses_cases.security.update_person_uc import UpdatePersonUC


@pytest.fixture
def rols():

    admin = Rol('001', 'ADMIN', True)
    teacher = Rol('001', 'PROFESOR', True)

    return [admin, teacher]


@pytest.fixture
def users(rols):
    user1 = User('001', 'saitama', 'saitama@saitama', '123456', 'https://i.pinimg.com/originals/51/3b/b7/513bb7305275cb7abba98af4b0aeb88e.jpg', False, True, rols[0])
    user2 = User('002', 'tatsumaki', 'tatsumaki@tatsumaki', '123456', 'https://i.pinimg.com/originals/18/2d/fa/182dfa3a6e441f8ce2717ecb2c946b0d.jpg', False, True, rols[1])
    return [user1, user2]


@pytest.fixture
def person(users):
    id = uuid.uuid4()
    lastname = 'Saitama'
    firstname = 'Saitama'
    position = 'Hero'
    avatar = 'https://i.pinimg.com/originals/3d/cb/ac/3dcbac445e57977db9c548265af1be22.jpg'
    person = Person('001', lastname, firstname, position, avatar, True, users[0])

    return person


def test_when_update_person_and_update_is_successfull(person):

    lastname = 'Kamado'
    firstname = 'Tanjiro'
    position = 'Demon Slayer'
    avatar = 'http://pm1.narvii.com/7232/5b7b4860193606793c45c18861935518f31b143fr1-863-720v2_uhq.jpg'
    username = 'tanjiro'
    email = 'tanjiro@tanjiro'

    person.lastname = 'Kamado'
    person.firstname = 'Tanjiro'
    person.position = 'Demon Slayer'
    person.avatar = 'http://pm1.narvii.com/7232/5b7b4860193606793c45c18861935518f31b143fr1-863-720v2_uhq.jpg'
    person.user.username = 'tanjiro'
    person.user.email = 'tanjiro@tanjiro'

    repo = mock.Mock()

    repo.get_by_username_or_email.return_value = None
    repo.hash_password.return_value = '654321'
    repo.save.return_value = person

    uc_person = UpdatePersonUC(repo)
    person_updated = uc_person.update(person)

    assert person_updated.id == '001'
    assert person_updated.lastname == lastname
    assert person_updated.firstname == firstname
    assert person_updated.position == position
    assert person_updated.avatar == avatar
    assert person_updated.status == True
    assert person_updated.user.username == username



def test_when_update_person_and_exist_user(person):

    repo = mock.Mock()

    repo.get_by_username_or_email.return_value = person.user
    repo.save.side_effect = PersonException('update: username or email in use')

    uc_person = UpdatePersonUC(repo)

    with pytest.raises(PersonException):
        result = uc_person.update(person)


