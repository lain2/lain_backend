import uuid
import pytest
from unittest import mock

from app.core.models.rol import Rol
from app.core.models.user import User
from app.core.models.person import Person
from app.core.exceptions.person_exception import PersonException
from app.core.uses_cases.security.get_a_person_uc import GetAPersonUC


@pytest.fixture
def rols():

    admin = Rol('001', 'ADMIN', True)
    teacher = Rol('001', 'PROFESOR', True)

    return [admin, teacher]


@pytest.fixture
def users(rols):
    user1 = User('001', 'saitama', 'saitama@saitama', '123456', 'https://i.pinimg.com/originals/51/3b/b7/513bb7305275cb7abba98af4b0aeb88e.jpg', False, True, rols[0])
    user2 = User('002', 'tatsumaki', 'tatsumaki@tatsumaki', '123456', 'https://i.pinimg.com/originals/18/2d/fa/182dfa3a6e441f8ce2717ecb2c946b0d.jpg', False, True, rols[1])
    return [user1, user2]


@pytest.fixture
def person(users):
    lastname = 'Saitama'
    firstname = 'Saitama'
    position = 'Hero'
    avatar = 'https://i.pinimg.com/originals/3d/cb/ac/3dcbac445e57977db9c548265af1be22.jpg'
    person = Person('001', lastname, firstname, position, avatar, True, users[0])

    return person


def test_when_get_a_person_by_email_and_exist(person):


    repo = mock.Mock()

    repo.get_by_username_or_email.return_value = person

    uc_person = GetAPersonUC(repo)
    person_finded = uc_person.get('saitama')


    assert person_finded.id == '001'
    assert person_finded.lastname == 'Saitama'
    assert person_finded.firstname == 'Saitama'
    assert person_finded.position == 'Hero'
    assert person_finded.status == True
    assert person_finded.user == person.user



def test_when_get_a_person_and_not_exist_user(person):

    repo = mock.Mock()

    repo.get_by_username_or_email.return_value = None

    uc_person = GetAPersonUC(repo)

    with pytest.raises(PersonException) as exception:
        result = uc_person.get('saitama@saitama')

    assert 'GetAPersonUC - get(): User not exist' in str(exception.value)


