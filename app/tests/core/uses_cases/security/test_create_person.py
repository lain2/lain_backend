import uuid
import pytest
from unittest import mock

from app.core.exceptions.person_exception import PersonException
from app.core.models.rol import Rol
from app.core.models.user import User
from app.core.models.person import Person
from app.core.repositories.security.repo_create_person import RepoCreatePerson
from app.core.uses_cases.security.create_person_uc import CreatePersonUC


@pytest.fixture
def rols():

    admin = Rol('001', 'ADMIN', True)
    teacher = Rol('001', 'PROFESOR', True)

    return [admin, teacher]


@pytest.fixture
def users(rols):
    user1 = User('001', 'saitama', 'saitama@saitama', '123456', 'https://i.pinimg.com/originals/51/3b/b7/513bb7305275cb7abba98af4b0aeb88e.jpg', False, True, rols[0])
    user2 = User('002', 'tatsumaki', 'tatsumaki@tatsumaki', '123456', 'https://i.pinimg.com/originals/18/2d/fa/182dfa3a6e441f8ce2717ecb2c946b0d.jpg', False, True, rols[1])
    return [user1, user2]


def test_when_create_person_and_saved_is_successfull(users):

    id = uuid.uuid4()
    lastname = 'Saitama'
    firstname = 'Saitama'
    position = 'Hero'
    avatar = 'https://i.pinimg.com/originals/3d/cb/ac/3dcbac445e57977db9c548265af1be22.jpg'

    person = Person(None, lastname, firstname, position, avatar, True, users[0])

    repo = mock.Mock()

    repo.exist_person.return_value = None
    repo.hash_password.return_value = '654321'
    repo.save.return_value = person

    uc_person = CreatePersonUC(repo)
    person_saved = uc_person.save(person)

    assert person_saved.id == None
    assert person_saved.lastname == lastname
    assert person_saved.firstname == firstname
    assert person_saved.position == position
    assert person_saved.avatar == avatar
    assert person_saved.status == True
    assert person_saved.user == users[0]



def test_when_create_person_hash_password_is_correct(users):

    id = uuid.uuid4()
    lastname = 'Saitama'
    firstname = 'Saitama'
    position = 'Hero'
    avatar = 'https://i.pinimg.com/originals/3d/cb/ac/3dcbac445e57977db9c548265af1be22.jpg'
    user = users[0]

    person = Person(id, lastname, firstname, position, avatar, True, user)

    repo = mock.Mock()

    repo.exist_person.return_value = None
    repo.hash_password.return_value = '654321'
    repo.save.return_value = person

    uc_person = CreatePersonUC(repo)
    person_saved = uc_person.save(person)

    assert person_saved.user.password == '654321'

def test_when_create_person_and_exist_user(users):

    id = uuid.uuid4()
    lastname = 'Saitama'
    firstname = 'Saitama'
    position = 'Hero'
    avatar = 'https://i.pinimg.com/originals/3d/cb/ac/3dcbac445e57977db9c548265af1be22.jpg'

    person = Person(id, lastname, firstname, position, avatar, True, users[0])

    repo = mock.Mock()

    repo.exist_person.return_value = users[0]
    repo.save.side_effect = PersonException('save: User exist')

    uc_person = CreatePersonUC(repo)

    with pytest.raises(PersonException):
        result = uc_person.save(person)


