# import uuid
# from unittest import mock

# import pytest
# from app.core.models.user import User
# from app.core.models.rol import Rol
# from app.core.exceptions.user_exception import UserException
# from app.core.uses_cases.security.create_user_uc import CreateUserUC


# def test_when_create_user_not_exist_saved_successfull():

#     id = uuid.uuid4()
#     name = "Jose"
#     email = "aaron.ariperto@gmail.com"
#     password = "123456"
#     avatar = "https://i.pinimg.com/736x/76/d4/f3/76d4f37dbb28d09e14d46468087d5f15.jpg"
#     rol = Rol('123', 'ADMIN')
#     google = False

#     user = User(id, name, email, password, avatar, google, rol)
#     repo = mock.Mock()
#     repo.save.return_value = user
#     repo.get_by_email.return_value = None

#     uc_user = CreateUserUC(repo)
#     result = uc_user.save(user)

#     repo.save.assert_called_with(user)

#     assert result == user



# def test_when_create_study_plan_and_exist_raised():

#     id = uuid.uuid4()
#     name = "Jose"
#     email = "aaron.ariperto@gmail.com"
#     password = "123456"
#     avatar = "https://i.pinimg.com/736x/76/d4/f3/76d4f37dbb28d09e14d46468087d5f15.jpg"
#     rol = Rol('123', 'ADMIN')
#     google = False

#     user = User(id, name, email, password, avatar, google, rol)

#     message = "UserCreateUseCase: User exist with same email"

#     repo = mock.Mock()
#     repo.get_by_email.return_value = user
#     repo.save.side_effect = UserException(message)

#     uc_user = CreateUserUC(repo)

#     with pytest.raises(UserException):
#         result = uc_user.save(user)

