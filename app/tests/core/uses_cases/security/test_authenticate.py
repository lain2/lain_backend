# import uuid
# from unittest import mock

# import pytest
# from app.core.models.rol import Rol
# from app.core.models.user import User
# from app.core.exceptions.user_exception import UserNotExistException, UserInvalidPasswordException
# from app.core.uses_cases.security.authenticate_uc import AuthenticateUC
# from fastapi.encoders import jsonable_encoder


# @pytest.fixture(scope="function")
# def user_data():
#     id = uuid.uuid4()
#     username = "kamado"
#     email = "kamado@gmail.com"
#     password = "tanjiro"
#     avatar = "https://i.pinimg.com/736x/76/d4/f3/76d4f37dbb28d09e14d46468087d5f15.jpg"
#     rol = Rol('123', 'ADMIN')
#     google = False
#     user = User(id, username, email, password, avatar, google, rol)
#     return user


# def test_when_authenticate_and_is_successfull(user_data):

#     repo = mock.Mock()
#     repo.get_user_by_username.return_value = user_data
#     repo.verify_password.return_value = True

#     uc_authenticate = AuthenticateUC(repo)
#     result = uc_authenticate.authenticate("kamado", "tanjiro")

#     assert result == user_data



# def test_when_authenticate_and_username_invalid():

#     repo = mock.Mock()
#     repo.get_user_by_username.return_value = None
#     repo.verify_password.return_value = True

#     uc_authenticate = AuthenticateUC(repo)

#     with pytest.raises(UserNotExistException):
#         result = uc_authenticate.authenticate("kamado", "tanjiro")

# def test_when_authenticate_and_password_invalid(user_data):

#     repo = mock.Mock()
#     repo.get_user_by_username.return_value = user_data
#     repo.verify_password.return_value = False

#     uc_authenticate = AuthenticateUC(repo)

#     with pytest.raises(UserInvalidPasswordException):
#         result = uc_authenticate.authenticate("kamado", "tanjiro")
