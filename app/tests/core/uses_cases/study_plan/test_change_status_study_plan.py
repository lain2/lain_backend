# import uuid
# import pytest
# from unittest import mock

# from app.core.exceptions.study_plan_exception import StudyPlanException
# from app.core.models.study_plan import StudyPlan
# from app.core.uses_cases.study_plan.change_status_study_plan_uc import ChangeStatusStudyPlanUC

# def test_when_change_status_and_is_true_change_to_false_successfull():

#     study_plan = StudyPlan('0001', '001', 'Lic. en sistemas', '')

#     repo = mock.Mock()
#     repo.get_by_id.return_value = study_plan
#     repo.update_status.return_value = True

#     uc_change_status = ChangeStatusStudyPlanUC(repo)
#     result = uc_change_status.change_status(study_plan.id)

#     assert result == True


# def test_when_change_status_and_not_exist_raised():

#     study_plan = StudyPlan('0001', '001', 'Lic. en sistemas', '')

#     repo = mock.Mock()
#     repo.get_by_id.return_value = None

#     uc_change_status = ChangeStatusStudyPlanUC(repo)

#     with pytest.raises(StudyPlanException):
#         result = uc_change_status.change_status(study_plan.id)
