# import uuid
# from unittest import mock

# import pytest
# from app.core.models.study_plan import StudyPlan
# from app.core.exceptions.study_plan_exception import StudyPlanException
# from app.core.uses_cases.study_plan.create_study_plan_uc import CreateStudyPlanUC


# def test_when_create_study_plan_not_exist_saved_successfull():

#     study_plan = StudyPlan(uuid.uuid4(), "071/08", "name", "description")

#     repo = mock.Mock()
#     repo.save.return_value = study_plan
#     repo.get_by_code.return_value = None

#     uc_study_plan = CreateStudyPlanUC(repo)
#     result = uc_study_plan.save(study_plan)

#     repo.save.assert_called_with(study_plan)
#     assert result == study_plan



# def test_when_create_study_plan_and_exist_raised():

#     study_plan = StudyPlan(uuid.uuid4(), "071/08", "name", "description")

#     repo = mock.Mock()
#     repo.get_by_code.return_value = study_plan
#     message = "StudyPlanCreateUseCase: Study Plan exist"
#     repo.save.side_effect = StudyPlanException(message)
#     repo.save.return_value = study_plan

#     uc_study_plan = CreateStudyPlanUC(repo)

#     with pytest.raises(StudyPlanException):
#         result = uc_study_plan.save(study_plan)
