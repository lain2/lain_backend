# import uuid
# import pytest
# from unittest import mock

# from app.core.exceptions.study_plan_exception import StudyPlanException
# from app.core.models.study_plan import StudyPlan
# from app.core.uses_cases.study_plan.update_study_plan_uc import UpdateStudyPlanUC


# def test_when_update_study_plan_and_not_exist_other_with_same_code_successfull():

#     study_plan = StudyPlan('0001', '001', 'Lic. en sistemas', '')

#     repo = mock.Mock()
#     repo.get_by_code.return_value = None
#     repo.update.return_value = study_plan

#     uc_update = UpdateStudyPlanUC(repo)
#     result = uc_update.update(study_plan)

#     repo.update.assert_called_with(study_plan)
#     assert result == study_plan


# def test_when_update_study_plan_and_exist_other_with_same_code_raised():

#     study_plan = StudyPlan(uuid.uuid4(), "071/08", "Lic. en sistemas", "description")

#     repo = mock.Mock()
#     repo.get_by_code.return_value = study_plan
#     message = "UpdateStudyPlanUC: StudyPlan exist with same code"
#     repo.update.side_effect = StudyPlanException(message)

#     uc_update = UpdateStudyPlanUC(repo)

#     with pytest.raises(StudyPlanException):
#         result = uc_update.update(study_plan)
