# import uuid
# from unittest import mock

# import pytest
# from app.core.models.study_plan import StudyPlan
# from app.core.uses_cases.study_plan.list_all_study_plan_uc import ListAllStudyPlanUC


# @pytest.fixture(scope='function')
# def study_plan_data():
#     study_plan_1 = StudyPlan('f853578c-fc0f-4e65-81b8-566c5dffa35a', '001', 'Lic. en sistemas', '')
#     study_plan_2 = StudyPlan('f853578c-fc0f-4e65-81b8-566c5dffa35b', '002', 'Lic. en sistemas', '')
#     return [ study_plan_1,
#             study_plan_2 ]


# def test_when_list_all_study_plan_and_exist_get_successfull(study_plan_data):

#     repo = mock.Mock()
#     repo.get_all.return_value = study_plan_data

#     uc_list_all = ListAllStudyPlanUC(repo)
#     result = uc_list_all.get_all()

#     assert result == study_plan_data


# def test_when_list_all_study_plan_and_is_empty_get_successfull():

#     repo = mock.Mock()
#     repo.get_all.return_value = []

#     uc_list_all = ListAllStudyPlanUC(repo)
#     result = uc_list_all.get_all()

#     assert result == []
