from .rol import Rol

class User:

    def __init__(self, id: str, username: str, email: str, password: str,
                 avatar: str, google: bool, status: bool, rol: Rol):
        self.id = id
        self.username = username
        self.email = email
        self.password = password
        self.avatar = avatar
        self.google = google
        self.status = status
        self.rol = rol

    def __eq__(self, other):
        id = self.id == other.id
        username = self.username == other.username
        email = self.email == other.email

        return (id or username or email)
