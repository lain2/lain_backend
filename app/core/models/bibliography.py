class Bibliography:

    def __init__(self, id: str, name: str, isbn: str, picture: str, status:bool):
        self.id = id
        self.name = name
        self.isbn = isbn
        self.picture = picture
        self.status = status


    def __eq__(self, other):
        equal_id = self.id == other.id
        equal_isbn = self.isbn == other.isbn

        return equal_isbn or equal_id
