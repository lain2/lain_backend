from .study_plan import StudyPlan

class Subject:

    def __init__(self, id: str, name: str, code: str, regime: str,
                 hours_per_week: int, total_hours: int, status: bool, study_plan: StudyPlan):
        self.id = id
        self.name = name
        self.code = code
        self.regime = regime
        self.hours_per_week = hours_per_week
        self.total_hours = total_hours
        self.status = status
        self.study_plan = study_plan


    def __eq__(self, other):
        equals_code = self.code == other.code
        equals_id = self.id == other.id

        return equals_code or equals_id
