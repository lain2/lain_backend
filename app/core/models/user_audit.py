from datetime import date
from .date_audit import DateAudit

class UserAudit(DateAudit):

    def __init__(self, created_by: str, updated_by: str) -> None:
        DateAudit.__init__(self, date.today().isoformat(), date.today().isoformat())
        self.created_by = created_by
        self.updated_by = updated_by

    @classmethod
    def from_dict(cls, dictionary: dict) -> object:
        return cls(
            created_by=dictionary["created_by"],
            updated_by=dictionary["updated_by"],
        )

    def to_dict(self) -> dict:
        return {"created_by": self.created_by, "updated_by": self.updated_by}
