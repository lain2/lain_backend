class DateAudit:
    """
    Permite auditar la creacion y modificacion de un objeto.

    Atributos:
        created_at(str): fecha de creacion.
        updated_at(str): fecha de modificacion.
    """

    def __init__(self, created_at: str, updated_at: str) -> None:
        self.created_at = created_at
        self.updated_at = updated_at

    @classmethod
    def from_dict(cls, dictionary: dict) -> object:
        """Crea un objecto StudyPlan en base a un diccionario."""
        return cls(
            created_at=dictionary["created_at"],
            updated_at=dictionary["updated_at"],
        )

    def to_dict(self) -> dict:
        """Convierte el objeto a un diccionario."""
        return {"created_at": self.created_at, "updated_at": self.updated_at}
