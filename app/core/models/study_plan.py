class StudyPlan:

    def __init__(self, id: str, code: str, name: str, description: str, status: bool):
        self.id = id
        self.code = code
        self.name = name
        self.description = description
        self.status = status

    def __eq__(self, other):
        equals_id = self.id == other.id
        equals_code = self.code == other.code

        return equals_id or equals_code



