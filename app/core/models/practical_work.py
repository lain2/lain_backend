from .subject import Subject

class PracticalWork:

    def __init__(self, id: str, name: str, number: int, activity: str, materials: str,
                 practice_enviroment: str, status: bool, subject: Subject):
        self.id = id
        self.name = name
        self.number = number
        self.activity = activity
        self.materials = materials
        self.practice_enviroment = practice_enviroment
        self.status = True
        self.subject = subject



    def __eq__(self, other):
        equals_id = self.id == other.id
        equals_name = self.name == other.name
        return equals_id or equals_name
