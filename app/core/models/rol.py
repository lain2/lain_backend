class Rol:

    def __init__(self, id: str, name: str, status: bool):
        self.id = id
        self.name = name
        self.status = status


    def __eq__(self, other):
        equals_id = self.id == other.id
        equals_name = self.name == other.name
        return equals_id and equals_name

