from typing import List

from .bibliography import Bibliography

class Descriptor:

    def __init__(self, id: str, name: str, description: str, status: bool):
        self.id = id
        self.name = name
        self.description = description
        self.status = status
        self.bibliography = []


    def __eq__(self, other):
        return self.id == other.id

