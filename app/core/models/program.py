from typing import List

from .subject import Subject

class Program:

    def __init__(self, id: str, minimal_content: str, fundaments: str, objectives: str,
                 methodology: str, evaluation: str, general_bibliography: str, status: bool,
                 subject: Subject):
        self.id = id
        self.minimal_content =minimal_content
        self.fundaments = fundaments
        self.objectives = objectives
        self.methodology = methodology
        self.evaluation = evaluation
        self.general_bibliography = general_bibliography
        self.status = status
        self.subject = subject


    def __eq__(self, other):
        return self.id == other.id
