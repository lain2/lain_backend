from typing import List
from .subject import Subject

class Unit:

    def __init__(self, id: str, number: int, name: str, description: str,
                 objectives: str, status: bool, subject: Subject):
        self.id = id
        self.number = number
        self.name = name
        self.description = description
        self.objectives = objectives
        self.status = status
        self.subject = subject


    def __eq__(self, other):
        equals_name = self.name == other.name
        equals_id = self.id == other.id
        return equals_id or equals_name
