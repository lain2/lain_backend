from .user import User


class Person:

    def __init__(self, id: str, lastname: str, firstname: str,
                 position: str, avatar: str, status:bool, user: User):

        self.id = id
        self.lastname = lastname
        self.firstname = firstname
        self.position = position
        self.avatar = avatar
        self.status = status
        self.user = user


    def __eq__(self, other):
        equals_id = self.id == other.id
        equals_user = self.user == other.user

        return equals_id or equals_user
