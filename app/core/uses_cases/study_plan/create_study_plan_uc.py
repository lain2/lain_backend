from app.core.exceptions.study_plan_exception import StudyPlanException
from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_create_study_plan import RepoCreateStudyPlan


class CreateStudyPlanUC:
    """
    Describe como se debe guardar un plan de estudio.

    Atributos:
        repo (RepoCreateStudyPlan): Implementacion del repositorio.
    """

    def __init__(self, repo: RepoCreateStudyPlan) -> None:
        self.repo = repo

    def save(self, study_plan: StudyPlan) -> StudyPlan:
        """Regla de guardado de un plan de estudio."""
        if self.repo.get_by_code(study_plan.code) is None:
            return self.repo.save(study_plan)

        raise StudyPlanException("CreateStudyPlanUC: Study Plan exist")
