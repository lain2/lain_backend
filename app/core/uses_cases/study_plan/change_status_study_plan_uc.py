from app.core.exceptions.study_plan_exception import StudyPlanException
from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_change_status_study_plan import (
    RepoChangeStatusStudyPlan,
)


class ChangeStatusStudyPlanUC:
    """Caso de uso para cambio de estado de un plan de estudio."""

    def __init__(self, repo: RepoChangeStatusStudyPlan) -> None:
        self.repo = repo

    def change_status(self, id: str) -> bool:

        study_plan = self.repo.get_by_id(id)

        if study_plan is None:
            raise StudyPlanException("ChangeStatusStudyPlanUC: Study Plan not exist")

        print(study_plan.status)
        study_plan.status = not (study_plan.status)
        print(study_plan.status)
        return self.repo.update_status(study_plan)
