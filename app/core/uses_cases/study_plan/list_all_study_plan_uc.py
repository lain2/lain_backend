from typing import List

from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_list_all_study_plan import RepoListAllStudyPlan


class ListAllStudyPlanUC:
    """
    Describre como se debe buscar un plan de estudio.

    Atributos:
        repo (RepoListAllStudyPlan): Implementacion del repositorio.
    """

    def __init__(self, repo: RepoListAllStudyPlan) -> None:
        self.repo = repo

    def get_all(self) -> List[StudyPlan]:
        """Regla de guardado de un plan de estudio."""
        return self.repo.get_all()
