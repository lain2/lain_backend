from app.core.exceptions.study_plan_exception import StudyPlanException
from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_update_study_plan import RepoUpdateStudyPlan


class UpdateStudyPlanUC:
    """
    La regla del caso de uso es que no debe existir otro plan de estudio.
    con el mismo codigo a modificar.

    Atributos:
        repo (RepoUpdateStudyPlan): Implementacion de la interfaz.
    """

    def __init__(self, repo: RepoUpdateStudyPlan) -> None:
        self.repo = repo

    def update(self, study_plan: StudyPlan) -> StudyPlan:
        """Permite actualizar un plan de estudio."""
        studyPlanExist = self.repo.get_by_code(study_plan.code)

        if studyPlanExist is None:
            return self.repo.update(study_plan)
        if studyPlanExist.id == study_plan.id:
            return self.repo.update(study_plan)

        raise StudyPlanException("UpdateStudyPlanUC: StudyPlan exist with same code.")
