from typing import Optional, Any
from loguru import logger
from app.core.models.study_plan import StudyPlan
from app.core.exceptions.study_plan_exception import StudyPlanInvalidKey
from app.core.repositories.study_plan.repo_get_study_plan import RepoGetStudyPlan

from fastapi.encoders import jsonable_encoder


class GetStudyPlanUC:

    def __init__(self, repo: RepoGetStudyPlan) -> None:
        self.repo = repo


    def get_study_plan(self, key: str, value: str) -> StudyPlan:
        logger.info('get_study_plan_by_' + key)

        study_plan = None

        if key == 'id':
            study_plan = self.repo.get_study_plan_by_id(value)

        if key == 'code':
            study_plan = self.repo.get_study_plan_by_code(value)


        if study_plan is None:
            raise StudyPlanInvalidKey('GetStudyPlanUC: invalid key')

        return study_plan
