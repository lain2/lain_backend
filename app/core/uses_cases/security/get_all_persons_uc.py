from typing import List
from app.core.models.person import Person
from app.core.models.user import User
from app.core.repositories.security.repo_get_all_persons import RepoGetAllPersons
from app.core.exceptions.person_exception import PersonException

class GetAllPersonsUC:

    def __init__(self, repo: RepoGetAllPersons):
        self.repo = repo


    def get_all(self, username = None, status = None, rol = None) -> List[Person]:

        if username is not None:
            persons = self.repo.get_all_by_username(username)
        elif status is not None:
            persons = self.repo.get_all_by_status(status)
        elif rol is not None:
            persons = self.repo.get_all_by_rol(rol)
        else:
            persons = self.repo.get_all()

        return persons




