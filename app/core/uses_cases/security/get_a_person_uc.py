from typing import Optional
from app.core.models.person import Person
from app.core.models.user import User
from app.core.repositories.security.repo_get_a_person import RepoGetAPerson
from app.core.exceptions.person_exception import PersonException

class GetAPersonUC:

    def __init__(self, repo: RepoGetAPerson):
        self.repo = repo


    def get(self, user: str) -> Person:

        person = self.repo.get_by_username_or_email(user)

        if person is None:
            raise PersonException('GetAPersonUC - get(): User not exist')

        return person




