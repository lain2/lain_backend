from typing import Optional
from app.core.models.person import Person
from app.core.models.user import User
from app.core.repositories.security.repo_update_person import RepoUpdatePerson
from app.core.exceptions.person_exception import PersonException

class UpdatePersonUC:

    def __init__(self, repo: RepoUpdatePerson):
        self.repo = repo


    def update(self, person: Person) -> Person:

        if self.is_same_user(person):
            raise PersonException('update: username or email in use')

        person.user = self.password_hash(person.user)

        return self.repo.save(person)


    def is_same_user(self, person: Person) -> Optional[User]:
        user = self.repo.get_by_username_or_email(person)
        if user is None:
            return False
        return  user.id  == person.user.id


    def password_hash(self, user: User) -> User:
        password = user.password
        password_hash = self.repo.hash_password(user.password)
        user.password = password_hash

        return user

