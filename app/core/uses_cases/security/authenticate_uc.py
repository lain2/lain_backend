from typing import Optional
from app.core.exceptions.user_exception import UserNotExistException, UserInvalidPasswordException
from app.core.models.user import User
from app.core.repositories.security.repo_authenticate import RepoAuthenticate
from loguru import logger

class AuthenticateUC:

    def __init__(self, repo: RepoAuthenticate ):
        self.repo = repo


    def authenticate(self, username: str, password: str) -> User:
        logger.info('authenticate')
        user = self.repo.get_user_by_username(username)
        if user is None:
            logger.error('user not exit')
            raise UserNotExistException("AuthenticateUC: username invalid")
        if self.repo.verify_password(user,password) is False:
            logger.error('password invalid')
            raise UserInvalidPasswordException("AuthenticateUC: password invalid")
        return user

