from typing import Optional, Any
from app.core.models.user import User
from app.core.exceptions.user_exception import UserInvalidKey
from app.core.repositories.security.repo_get_user import RepoGetUser
from loguru import logger
from fastapi.encoders import jsonable_encoder


class GetUserUC:

    def __init__(self, repo: RepoGetUser) -> None:
        self.repo = repo


    def get_user(self, key: str, value: str) -> User:
        logger.info('get_user_by_' + key)

        user = None

        if key == 'id':
            user = self.repo.get_user_by_id(value)

        if key == 'username':
            user = self.repo.get_user_by_username(value)

        if key == 'email':
            user = self.repo.get_user_by_email(value)

        if user is None:
            raise UserInvalidKey('GetUserUC: invalid key')

        logger.info(jsonable_encoder(user))
        return user
