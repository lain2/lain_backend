from typing import Optional
from app.core.models.person import Person
from app.core.models.user import User
from app.core.repositories.security.repo_create_person import RepoCreatePerson
from app.core.exceptions.person_exception import PersonException

class CreatePersonUC:

    def __init__(self, repo: RepoCreatePerson):
        self.repo = repo


    def save(self, person: Person) -> Person:

        if self.exist_user(person.user):
            raise PersonException('save: User exist')


        person.user = self.password_hash(person.user)

        return self.repo.save(person)


    def exist_user(self, user: User) -> Optional[User]:
        user = self.repo.exist_person(user)
        return  user is not None


    def password_hash(self, user: User) -> User:
        password = user.password
        password_hash = self.repo.hash_password(user.password)
        user.password = password_hash

        return user

