from typing import Optional
from app.core.exceptions.rol_exception import RolException
from app.core.models.rol import Rol
from app.core.repositories.security.repo_get_rol_by_id import RepoGetRolById


class GetRolByIdUC:

    def __init__(self, repo: RepoGetRolById) -> None:
        self.repo = repo

    def get_by_id(self, id: str) -> Optional[Rol]:

        return self.repo.get_by_id(id)


