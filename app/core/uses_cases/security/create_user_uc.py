import uuid
from app.core.exceptions.user_exception import UserException
from app.core.models.user import User
from app.core.repositories.security.repo_create_user import RepoCreateUser
from loguru import logger


class CreateUserUC:

    def __init__(self, repo: RepoCreateUser) -> None:
        logger.info('Constructor')
        self.repo = repo

    def save(self, user: User) -> User:
        logger.info('save')
        if self.repo.get_by_email(user.email) is None:
            user.id = str(uuid.uuid4())
            return self.repo.save(user)

        logger.error('User exist')
        raise UserException("CreateUserUC: User exist")

