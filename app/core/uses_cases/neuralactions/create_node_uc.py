import uuid

from app.core.models.node import Node
from app.core.models.user import User
from app.core.exceptions.node_exception import NodeException
from app.core.exceptions.person_exception import PersonExistException, PersonUserIsNoneException
from app.core.repositories.neuralactions.repo_create_node import RepoCreateNode
from loguru import logger


class CreateNodeUC:

    def __init__(self, repo: RepoCreateNode) -> None:
        logger.info('__init__')
        self.repo = repo

    def create(self, node: Node) -> Node:
        logger.info('create')

        if node.id_neuralactions is None:
            logger.error('id_neuralactions is none')
            raise NodeException("CreateNodeUC: id_neuralactions is none")

        node.id = str(uuid.uuid4())
        node = self.repo.save(node)

        return node
