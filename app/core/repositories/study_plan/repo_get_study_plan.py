import abc
from typing import Optional, Any

from app.core.models.study_plan import StudyPlan


class RepoGetStudyPlan(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_study_plan_by_id(self, id: str) -> StudyPlan:
        raise NotImplementedError

    @abc.abstractmethod
    def get_study_plan_by_code(self, code: str) -> StudyPlan:
        raise NotImplementedError

