import abc
from typing import Optional

from app.core.models.study_plan import StudyPlan


class RepoUpdateStudyPlan(metaclass=abc.ABCMeta):
    """Metodos necesarios para actualizar un plan de estudio."""

    @abc.abstractmethod
    def update(self, study_plan: StudyPlan) -> StudyPlan:
        """Representa el guardado de un plan de estudio."""

    @abc.abstractmethod
    def get_by_code(self, code: str) -> Optional[StudyPlan]:
        """Representa la busqueda de un plan de estudio por su codigo."""
