import abc
from typing import Optional

from app.core.models.study_plan import StudyPlan


class RepoChangeStatusStudyPlan(metaclass=abc.ABCMeta):
    """Reglas para la actualizacion del estado de un plan de estudio."""

    @abc.abstractmethod
    def get_by_id(self, id: str) -> Optional[StudyPlan]:
        pass

    @abc.abstractmethod
    def update_status(self, study_plan: StudyPlan) -> bool:
        pass
