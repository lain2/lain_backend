import abc
from typing import Optional

from app.core.models.study_plan import StudyPlan


class RepoCreateStudyPlan(metaclass=abc.ABCMeta):
    """Reglas de negocio para guardar un plan de estudio."""

    @abc.abstractmethod
    def save(self, study_plan: StudyPlan) -> StudyPlan:
        """Representa el guardado de un plan de estudio."""

    @abc.abstractmethod
    def get_by_code(self, code: str) -> Optional[StudyPlan]:
        """Representa la busqueda de un plan de estudio por su codigo."""
