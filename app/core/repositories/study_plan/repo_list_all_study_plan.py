import abc
from typing import List

from app.core.models.study_plan import StudyPlan


class RepoListAllStudyPlan(metaclass=abc.ABCMeta):
    """Reglas de negocio para listar todos los planes de estudio."""

    @abc.abstractmethod
    def get_all(self) -> List[StudyPlan]:
        """Representa la busqueda de todos los planes de estudio."""
