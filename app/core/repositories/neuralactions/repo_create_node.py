import abc
from typing import Optional

from app.core.models.node import Node


class RepoCreateNode(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def save(self, node:  Node) ->  Node:
        raise NotImplementedError

