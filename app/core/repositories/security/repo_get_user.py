import abc
from typing import Optional, Any

from app.core.models.user import User


class RepoGetUser(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_user_by_id(self, id: str) -> User:
        raise NotImplementedError


    @abc.abstractmethod
    def get_user_by_username(self, username: str) -> User:
        raise NotImplementedError

    @abc.abstractmethod
    def get_user_by_email(self, email: str) -> User:
        raise NotImplementedError


