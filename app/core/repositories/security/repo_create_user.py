import abc
from typing import Optional

from app.core.models.user import User


class RepoCreateUser(metaclass=abc.ABCMeta):
    """Reglas de negocio para guardar un plan de estudio."""

    @abc.abstractmethod
    def save(self, user:  User) ->  User:
        raise NotImplementedError

    @abc.abstractmethod
    def get_by_email(self, code: str) -> Optional[User]:
        raise NotImplementedError
