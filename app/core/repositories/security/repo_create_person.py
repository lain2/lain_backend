import abc
from app.core.models.user import User
from app.core.models.person import Person

class RepoCreatePerson(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def exist_person(self, person: Person) -> User:
        pass

    @abc.abstractmethod
    def save(self, person: Person) -> Person:
        pass

    @abc.abstractmethod
    def hash_password(self, password: str) -> str:
        pass

