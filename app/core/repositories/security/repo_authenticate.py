import abc
from typing import Optional

from app.core.models.user import User


class RepoAuthenticate(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_user_by_username(self, email:  str) ->  User:
        raise NotImplementedError

    @abc.abstractmethod
    def verify_password(self, user: User, password:str) -> bool:
        raise NotImplementedError
