import abc
from app.core.models.user import User
from app.core.models.person import Person

class RepoGetAPerson(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_by_username_or_email(self, user: str) -> Person:
        pass

