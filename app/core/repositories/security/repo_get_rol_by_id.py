import abc
from typing import Optional

from app.core.models.rol import Rol


class RepoGetRolById(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_by_id(self, id: str) -> Optional[Rol]:
        raise NotImplementedError
