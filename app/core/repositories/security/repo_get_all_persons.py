import abc
from typing import List
from app.core.models.user import User
from app.core.models.person import Person

class RepoGetAllPersons(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_all(self) -> List[Person]:
        pass

    @abc.abstractmethod
    def get_all_by_status(self, status: bool) -> List[Person]:
        pass

    @abc.abstractmethod
    def get_all_by_username(self, username: str) -> List[Person]:
        pass

    @abc.abstractmethod
    def get_all_by_rol(self, rol: str) -> List[Person]:
        pass
