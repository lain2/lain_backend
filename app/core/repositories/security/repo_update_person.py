import abc
from app.core.models.person import Person
from app.core.models.user import User

class RepoUpdatePerson(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def update(self, person: Person) -> Person:
        pass

    @abc.abstractmethod
    def get_by_username_or_email(self, person: Person) -> User:
        pass

    @abc.abstractmethod
    def hash_password(self, password: str) -> str:
        pass
