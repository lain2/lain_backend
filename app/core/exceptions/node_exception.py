from loguru import logger

class NodeException(Exception):

    def __init__(self, message: str) -> None:

        logger.error(message)
        self.message = message
        super().__init__(self.message)


class ErrorToSaveNode(NodeException):

    def __init__(self, message: str) -> None:

        self.message = message
        super().__init__(self.message)
