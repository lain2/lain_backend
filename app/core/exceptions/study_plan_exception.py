from loguru import logger


class StudyPlanException(Exception):
    """
    Excepcion personalizada para la clase StudyPlan.

    Atributos:
        message(str): mensaje que figura cuando se genera un error en la clase.
    """

    def __init__(self, message: str) -> None:
        logger.error(message)
        self.message = message
        super().__init__(self.message)


class StudyPlanInvalidKey(StudyPlanException):

    def __init__(self, message: str) -> None:

        self.message = message
        super().__init__(self.message)
