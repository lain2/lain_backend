from loguru import logger

class UserException(Exception):

    def __init__(self, message: str) -> None:

        logger.error(message)
        self.message = message
        super().__init__(self.message)


class UserNotExistException(UserException):

    def __init__(self, message: str) -> None:

        self.message = message
        super().__init__(self.message)

class UserInvalidPasswordException(UserException):

    def __init__(self, message: str) -> None:

        self.message = message
        super().__init__(self.message)


class UserInvalidKey(UserException):
    def __init__(self, message: str) -> None:

        self.message = message
        super().__init__(self.message)

