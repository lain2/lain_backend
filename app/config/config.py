import secrets
import os
from .dev_config import dev
# from .prod_config import prod
from typing import Any, Dict, List, Optional, Union

from pydantic import (AnyHttpUrl, BaseSettings, EmailStr, HttpUrl, PostgresDsn,
                      validator)


settings = dev
# settings = prod
