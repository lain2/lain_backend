import httpx
import json
import requests
import os
from fastapi import HTTPException
from loguru import logger

from app.rest.service.neuralactions.auth import generate_headers
from app.config.config import settings

URL_WORKSPACES = settings.NEURAL_URL + '/nodes/scheme'


def create_in_neauralactiosn(scheme):
    headers = generate_headers()

    workspace = json.dumps(scheme)
    response = requests.post(URL_WORKSPACES, data=scheme, headers=headers)
    error = response.json()["error"]
    if error is not None:
        raise HTTPException(status_code=400, detail="CreateScheme: fail to create scheme " + scheme.name + " " + error)

    return response


def create_subject(workspace_id):
    data  = {
        "name": "materia",
        "fields": [
            { "name": "imagen", "hard_type": "picture", "pos": "0" },
            { "name": "nombre", "hard_type": "string", "pos": "1" },
            { "name": "codigo", "hard_type": "string", "pos": "2" },
            { "name": "regimen", "hard_type": "string", "pos": "3" },
            { "name": "horas_semanales", "hard_type": "string", "pos": "4" },
            { "name": "total_horas", "hard_type": "string", "pos": "5" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_year(workspace_id):
    data  = {
        "name": "año",
        "fields": [
            { "name": "numero", "hard_type": "string", "pos": "0" },
            { "name": "nombre", "hard_type": "string", "pos": "1" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_node(workspace_id):
    data  = {
        "name": "nodo",
        "fields": [
            { "name": "nombre", "hard_type": "string", "pos": "0" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_teacher(workspace_id):
    data  = {
        "name": "docente",
        "fields": [
            { "name": "imagen", "hard_type": "picture", "pos": "0" },
            { "name": "nombres", "hard_type": "string", "pos": "1" },
            { "name": "apellidos", "hard_type": "string", "pos": "2" },
            { "name": "email", "hard_type": "string", "pos": "3" },
            { "name": "cargo", "hard_type": "string", "pos": "4" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_program(workspace_id):
    data  = {
        "name": "programa",
        "fields": [
            { "name": "contenido_minimo", "hard_type": "html", "pos": "0" },
            { "name": "fundamentos", "hard_type": "html", "pos": "1" },
            { "name": "objetivos", "hard_type": "html", "pos": "2" },
            { "name": "metodologia", "hard_type": "html", "pos": "3" },
            { "name": "evaluacion", "hard_type": "html", "pos": "4" },
            { "name": "bibliografia_general", "hard_type": "html", "pos": "5" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_bibliography(workspace_id):
    data  = {
        "name": "bibliografia",
        "fields": [
            { "name": "imagen", "hard_type": "picture", "pos": "0" },
            { "name": "nombre", "hard_type": "string", "pos": "1" },
            { "name": "isbn", "hard_type": "string", "pos": "2" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_unit(workspace_id):
    data  = {
        "name": "unidad",
        "fields": [
            { "name": "numero", "hard_type": "string", "pos": "0" },
            { "name": "nombre", "hard_type": "string", "pos": "1" },
            { "name": "objetivos", "hard_type": "html", "pos": "2" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_practical_work(workspace_id):
    data  = {
        "name": "trabajo practico",
        "fields": [
            { "name": "numero", "hard_type": "string", "pos": "0" },
            { "name": "nombre", "hard_type": "string", "pos": "1" },
            { "name": "actividad", "hard_type": "html", "pos": "2" },
            { "name": "materiales", "hard_type": "html", "pos": "3" },
            { "name": "ambito_de_practica", "hard_type": "html", "pos": "4" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_minimal_content(workspace_id):
    data  = {
        "name": "contenido minimo",
        "fields": [
            { "name": "unidad", "hard_type": "string", "pos": "0" },
            { "name": "nombre", "hard_type": "string", "pos": "1" },
            { "name": "descripcion", "hard_type": "html", "pos": "2" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_descriptor(workspace_id):
    data  = {
        "name": "descriptor",
        "fields": [
            { "name": "nombre", "hard_type": "string", "pos": "0" },
            { "name": "descripcion", "hard_type": "html", "pos": "1" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_theme(workspace_id):
    data  = {
        "name": "tema",
        "fields": [
            { "name": "nombre", "hard_type": "string", "pos": "0" },
            { "name": "descripcion", "hard_type": "html", "pos": "1" },
        ],
        "workspaces": { "active_workspace_id": workspace_id }}
    create_in_neauralactiosn(data)


def create_all_schemes(workspace_id):
    try:
        create_subject(workspace_id)
        create_year(workspace_id)
        create_node(workspace_id)
        create_teacher(workspace_id)
        create_program(workspace_id)
        create_bibliography(workspace_id)
        create_unit(workspace_id)
        create_practical_work(workspace_id)
        create_minimal_content(workspace_id)
        create_descriptor(workspace_id)
        create_theme(workspace_id)
    except (Exception):
        raise Exception("Fail to create all schemes")
