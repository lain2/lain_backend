import httpx
import json
import requests
import os
from fastapi import HTTPException
from loguru import logger

from app.rest.service.neuralactions.auth import generate_headers
from app.config.config import settings

URL_NODES = settings.NEURAL_URL + 'nodes'


def create_in_neauralactions(node):
    logger.info('create_in_neuralactions')
    headers = generate_headers()

    logger.info('1')
    response = requests.post(URL_NODES, data=node, headers=headers)
    if response.status_code != 201:
        raise HTTPException(status_code=500, detail="CreateNode: fail to create node in neuralactions: " )

    logger.warning(response.json()['obj_data'])

    return response.json()['obj_data']

