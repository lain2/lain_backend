import httpx
import json
import requests
import os
from fastapi import HTTPException
from loguru import logger

from app.rest.service.neuralactions.auth import generate_headers
from app.config.config import settings

URL_NODES = settings.NEURAL_URL + 'tags'


def put_tag_node(data):
    logger.info('update_tag_node')
    headers = generate_headers()

    data = json.dumps(data)
    logger.info(data)
    logger.info(URL_NODES)
    response = requests.put(URL_NODES, data=data, headers=headers)
    logger.warning(response.json())

    if response.status_code < 200 or response.status_code > 400:
        raise HTTPException(status_code=500, detail="put tag node error" )


    return response.json()
