import httpx
import json
import requests
import os
from fastapi import HTTPException
from loguru import logger

from app.rest.service.neuralactions.auth import generate_headers
from app.config.config import settings
from app.rest.service.neuralactions.create_schemes import create_all_schemes

URL_WORKSPACES = settings.NEURAL_URL + 'workspaces'


def create(workspace):
    headers = generate_headers()
    exist = validate(headers, workspace["name"])
    if exist:
        raise HTTPException(
            status_code=400,
            detail="CreateStudyPlanUC: Study Plan exist in NeuralActions",
        )
    workspace = json.dumps(workspace)
    logger.error(workspace)
    response = requests.post(URL_WORKSPACES, data=workspace, headers=headers)

    error = response.json()["error"]
    if error is not None:
        raise HTTPException(status_code=400, detail="CreateStudyPlanUC: " + error)

    workspace_id = response.json()["id"]
    create_all_schemes(workspace_id)

    return response

def validate(headers, name: str):
    name = name.lower().strip()
    logger.error(name)
    logger.error(URL_WORKSPACES)
    logger.error(headers)
    response = requests.get(URL_WORKSPACES, headers=headers)
    logger.error(response.json())
    workspaces = response.json()["own"]
    names = [(w["name"]).lower().strip() for w in workspaces]
    return name in names
