import requests
from datetime import datetime
from app.config.config import settings
from app.data.crud.update_token_neural import update_token
from app.data.deps import get_db
from app.data.entities.token_neural_entity import TokenNeuralEntity

URL_LOGIN = settings.NEURAL_URL + '/login'

def login(user):
    response = requests.post(URL_LOGIN, data=user)
    return response


def get_paypal_token():

    user = {"email": settings.NEURAL_USERNAME, "password": settings.NEURAL_PASSWORD}
    login_json = login(user).json()
    token = login_json["access_token"]
    return token


def generate_headers():
    # token = get_token()
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRmNjE5YjdlNjczNDM5MGM3ZmU5Y2JkMTkyYjM3NGExZmQxMzc4N2Y2ZjVjNzA3Y2FiMDEyNTQyNDczYjdmZThiOWYxZWU0OTY5ODcxMGZiIn0.eyJhdWQiOiIxIiwianRpIjoiNGY2MTliN2U2NzM0MzkwYzdmZTljYmQxOTJiMzc0YTFmZDEzNzg3ZjZmNWM3MDdjYWIwMTI1NDI0NzNiN2ZlOGI5ZjFlZTQ5Njk4NzEwZmIiLCJpYXQiOjE2MjYwMzYxNDMsIm5iZiI6MTYyNjAzNjE0MywiZXhwIjoxNjU3NTcyMTQzLCJzdWIiOiIzMyIsInNjb3BlcyI6W119.JHmFIblbjyH0V5GqM2crU6SaLPk-RdYPcjewaCd-ovKnWtMDvPa4cGS4srG_yWQzyqAKLK8PNbS3bDx7cc6sePyQlRnnNcULKOPk9QSPfBhuJmBmwteB7WRVviE80zViPG_EYZapMu6Eup8Bs8BJe1SC0QWRXZormiV-h04BPa2EDtm97vilc841StjCTaPLgZgeo6YGg4H44_QyWSH0LAkSaSKx522dezq0_5xDI6Rdeztb1QMX9J99qB1F1j9Dcr02m-2_OkKY8cGVObCKRJxJsnAROhv76L5LF5kdHS_kHp4YkQIhzYwdvXmYjXKU4Jv3zF_Ox8cL-sUz8yy_R31077hoWd1XDgAH3Edhe9-qfeiwextsAaQVGhRhnMWTZtvkbm3lMWSwXXWHTV4EiaTTUqdQAjOaGZYNi_en1p1OobGAYzdNLcNFekE4WN8IdHV5M2P4VHX9GuAvf5oWN4aSL71agx2SQ-2RhL-eSn1a522p-KehgWx3gyE8Xt2c95LL_d0yj1CcnrwF_-lKlr6y0unII3YRsSTxsyXC9zhwmh4DWLts1eLiG-2S0snUCZPWsy5ryjN8yRhNAJhDG7HudXAAATaAcxgqQi9OfVtoJcBe0aIclf7ZU46WmpkiDmOT74UPLW9Q5OYsvg1IsDDxjMdgwbfymjSUXRRNNfs'
    return {"Content-Type": "application/json", "Authorization": "Bearer " + token}


def get_token() -> str:
    db = next(get_db(), None)  # type: ignore
    token = update_token.get_token(db)
    if is_valid_token(token):
        return token.token
    else:
        token.token = get_paypal_token()
        token = update_token.update(db=db, token=token)
        return token.token

def is_valid_token(token: TokenNeuralEntity):
    """Valida si el token no se vencio."""
    next(get_db(), None)  # type: ignore
    actual_date = datetime.now()
    diff = actual_date - token.updated_at
    time = diff.total_seconds()
