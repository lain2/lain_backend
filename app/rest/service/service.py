import os
import httpx
from fastapi import HTTPException
from loguru import logger
from app.config.config import settings
from app.core.models.user import User
from fastapi.encoders import jsonable_encoder
url = settings.SERVER_HOST
route = settings.ROUTE


def generate_token(user):
    logger.info('generate_token')
    headers =  {'Content-Type': 'application/x-www-form-urlencoded'}
    body = {'username': user.username, 'password': user.password}
    token = httpx.post(f'{url}{route}/auth/token', headers=headers, data=body)
    logger.error(token)
    return token





def get_user_by_id(id: str, token: str):
    logger.error(token)
    logger.info('get_user_by_id')
    params = {'key': 'id', 'value': id}
    headers = {'Authorization': 'Bearer ' + token}
    r = httpx.get(f'{url}{route}/user', params=params, headers=headers)
    if r.status_code != 200:
        logger.error('status code')
        raise HTTPException(
            status_code=r.status_code, detail="fail get user"
        )
    logger.info(r.json())
    user = User.from_dict(r.json())
    return r.json()






