import httpx
from fastapi import HTTPException
from loguru import logger
from app.config.config import settings
from app.core.models.study_plan import StudyPlan
from fastapi.encoders import jsonable_encoder
from typing import Any

url = settings.SERVER_HOST
route = settings.ROUTE



def post_node(req, token: str):
    logger.info('post_node')
    headers = {'Authorization': 'Bearer ' + token}
    r = httpx.post(f'{url}{route}/neuralactions/', headers=headers, json=req)
    if r.status_code != 200:
        logger.error('status code')
        raise HTTPException(
            status_code=r.status_code, detail="fail get study plan"
        )
    return r.json()






