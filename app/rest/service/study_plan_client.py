import httpx
from fastapi import HTTPException
from loguru import logger
from app.config.config import settings
from app.core.models.study_plan import StudyPlan
from fastapi.encoders import jsonable_encoder

url = settings.SERVER_HOST
route = settings.ROUTE



def get_study_plan_by_id(id: str, token: str):
    logger.info('get_study_plan_by_id')
    params = {'key': 'id', 'value': id}
    headers = {'Authorization': 'Bearer ' + token}
    r = httpx.get(f'{url}{route}/study-plan/get', params=params, headers=headers)
    if r.status_code != 200:
        logger.error('status code')
        raise HTTPException(
            status_code=r.status_code, detail="fail get study plan"
        )
    return r.json()






