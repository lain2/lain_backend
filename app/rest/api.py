from fastapi import APIRouter

from app.rest.routes.study_plan import router as study_plan_router
from app.rest.routes.security.person import router as person_router
from app.rest.routes.neuralactions import router as neuralactions_router
from .routes.security import (auth_rest, user_rest)

api_router = APIRouter()

api_router.include_router(study_plan_router, prefix="/study-plan")
api_router.include_router(user_rest.router, prefix="/user", tags=["User"])
api_router.include_router(auth_rest.router, prefix="/auth", tags=["Auth"] )
api_router.include_router(person_router, prefix="/person")
api_router.include_router(neuralactions_router, prefix="/neuralactions")

print(api_router.prefix)
