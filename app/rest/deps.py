from datetime import datetime

from fastapi import Depends, HTTPException, Header
from fastapi.security import OAuth2PasswordBearer

from app.data import deps
from app.data.crud.update_token_neural import update_token
from app.data.crud.crud_user import crud_user
from app.data.entities.token_neural_entity import TokenNeuralEntity

# from app.rest.service.neuralactions.auth import get_paypal_token
from app.data.deps import get_db
from app.core.models.user import User
from jose import jwt
from app.config.config import settings
from app.core.models.security import ALGORITHM
from loguru import logger
from fastapi.encoders import jsonable_encoder
from app.rest.mapper.user_mapper import UserMapperDTO

reusable_oauth2 = OAuth2PasswordBearer(tokenUrl='/api/v1/developer/auth/token')





def get_current_user(
    db = Depends(get_db),
    token: str = Depends(reusable_oauth2)
) -> User:
    logger.info('get_current_user')
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[ALGORITHM])
        user_dto = payload['user']
        user = UserMapperDTO.DTOToCore(user_dto)
        return user

    except (jwt.JWTError):
        logger.error('invalidCredentials')
        raise HTTPException(
            status_code=403,
            detail="Could not validate credentials",
        )


def is_admin_rol(user: str = Depends(get_current_user)):
    logger.info('is_admin_rol')
    try:
        rol = user.rol
        if rol.name == 'ADMIN':
            return user
        logger.error('user is not a ADMIN')
        raise HTTPException(
            status_code=403,
            detail="User don't have privileges",
        )

    except (jwt.JWTError):
        logger.error('user is not a ADMIN')
        raise HTTPException(
            status_code=403,
            detail="User don't have privileges",
        )

# def get_token() -> str:
#     """Retorna un token desde neuralactions."""
#     db = next(deps.get_db(), None)  # type: ignore
#     token = update_token.get_token(db)
#     logger.error(token)
#     if is_valid_token(token):
#         return token.token
#     else:
#         token.token = get_paypal_token()
#         token = update_token.update(db=db, token=token)
#         return token.token


# def is_valid_token(token: TokenNeuralEntity):
#     """Valida si el token no se vencio."""
#     next(deps.get_db(), None)  # type: ignore
#     actual_date = datetime.now()
#     diff = actual_date - token.updated_at
#     time = diff.total_seconds()



