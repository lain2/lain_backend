from typing import Optional
from pydantic import BaseModel


class NodeSchema(BaseModel):
    id: Optional[str]
    id_neuralactions: str
    node_type: str

    def __setitem__(self, item):
        return setattr(self, item)

    def __getitem__(self, item):
        return getattr(self, item)

