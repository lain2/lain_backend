from typing import Optional
from pydantic import BaseModel
from .user_schema import UserSchema
from .study_plan import GetStudyPlanScheme


class PersonSchema(BaseModel):
    id: Optional[str]
    firstname: str
    lastname: str
    position: str
    avatar: str
    status: bool
    user_id: Optional[str]
    user: Optional[UserSchema]
    study_plan_id: Optional[str]
    study_plan: Optional[GetStudyPlanScheme]

    def __setitem__(self, item):
        return setattr(self, item)

    def __getitem__(self, item):
        return getattr(self, item)

class PersonInDB(PersonSchema):

    class Config:

        orm_mode = True
