from pydantic import BaseModel


class StudyPlanBase(BaseModel):
    """Esquema base de plan de estudio."""

    code: str
    name: str
    description: str


class SPInDB(StudyPlanBase):
    """Esquema de plan de estudio en base de datos."""

    class Config:
        """Configuracion de SPInDB."""

        orm_mode = True


class CreateStudyPlanScheme(SPInDB):
    """Esquema base para creacion de plan de estudio."""


class GetStudyPlanScheme(SPInDB):
    id: str
    status: bool

    """Esquema para traer un plan de estudio."""


class UpdateStudyPlanScheme(SPInDB):
    """Esquema base para actualizar plan de estudio."""

    id: str


class UpdateStatusStudyPlan(BaseModel):

    id: str
    updated: bool
