from typing import Optional, Any
from pydantic import BaseModel
from .rol_schema import RolSchema


class UserQuerySchema(BaseModel):
    key: str
    value: Any

class UserSchema(BaseModel):
    id: Optional[str]
    username: Optional[str]
    email: Optional[str]
    password: Optional[str]
    avatar: Optional[str]
    google: Optional[bool]
    status: Optional[bool]
    rol: Optional[RolSchema]

    def __getitem__(self, item):
        return getattr(self, item)


class UserInDB(UserSchema):

    class Config:

        orm_mode = True
