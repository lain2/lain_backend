from pydantic import BaseModel
from typing import Optional


class RolSchema(BaseModel):
    id: str
    name: Optional[str]

    def __getitem__(self, item):
        return getattr(self, item)


class RolInDB(RolSchema):

    class Config:

        orm_mode = True
