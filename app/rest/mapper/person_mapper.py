from app.core.models.person import Person

from app.core.models.rol import Rol
from app.core.models.user import User
from ..schemas.person_schema import PersonSchema
from ..schemas.rol_schema import RolSchema
from .user_mapper import UserMapperDTO
from .study_plan_mapper import StudyPlanMapperDTO
from fastapi.encoders import jsonable_encoder
from loguru import logger


class PersonMapperDTO:

    @staticmethod
    def DTOToCore(person_schema: PersonSchema) -> Person:
        logger.info('DTOToCore')
        person_dict = jsonable_encoder(person_schema)
        person = Person.from_dict(person_dict)
        return person

    @staticmethod
    def coreToDTO(person: Person) -> PersonSchema:
        logger.info('coreToDTO')
        user = UserMapperDTO.coreToDTO(person.user)
        study_plan = StudyPlanMapperDTO.coreToDTO(person.study_plan)
        logger.warning(jsonable_encoder(study_plan))
        logger.info(type(study_plan))
        personSchema = PersonSchema(
            id=person.id,
            firstname=person.firstname,
            lastname=person.lastname,
            position=person.position,
            avatar=person.avatar,
            status=person.status,
            user=user,
            study_plan=study_plan
        )
        logger.error(jsonable_encoder(personSchema))
        return personSchema
