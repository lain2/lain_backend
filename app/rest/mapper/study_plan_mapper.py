from app.core.models.study_plan import StudyPlan

from ..schemas.study_plan import GetStudyPlanScheme


class StudyPlanMapperDTO:
    @staticmethod
    def DTOToCore(scheme: GetStudyPlanScheme) -> StudyPlan:
        study_plan = StudyPlan(scheme.id, scheme.code, scheme.name, scheme.description)
        study_plan.status = scheme.status
        return study_plan

    @staticmethod
    def coreToDTO(study_plan: StudyPlan) -> GetStudyPlanScheme:
        scheme = GetStudyPlanScheme(
            id=study_plan.id,
            code=study_plan.code,
            name=study_plan.name,
            description=study_plan.description,
            status=study_plan.status,
        )
        return scheme
