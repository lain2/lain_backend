from app.core.models.rol import Rol
from app.core.models.user import User
from ..schemas.user_schema import UserSchema
from ..schemas.rol_schema import RolSchema
from .rol_mapper import RolMapperDTO
from fastapi.encoders import jsonable_encoder
import json 
from loguru import logger


class UserMapperDTO:

    @staticmethod
    def DTOToCore(userSchema: UserSchema) -> User:
        logger.info('DTOToCore')
        userDict = jsonable_encoder(userSchema)
        user = User.from_dict(userDict)
        return user

    @staticmethod
    def coreToDTO(user: User) -> UserSchema:
        logger.info('coreToDTO')
        rol = RolMapperDTO.coreToDTO(user.rol)
        logger.error(jsonable_encoder(rol))
        userSchema = UserSchema(
            id=user.id,
            username=user.username,
            email=user.email,
            password=user.password,
            avatar=user.avatar,
            google=user.google,
            status=user.status,
            rol=rol
        )
        return userSchema
