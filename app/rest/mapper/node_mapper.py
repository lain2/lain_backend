from app.core.models.node import Node

from app.rest.schemas.node_schema import NodeSchema


from app.core.models.rol import Rol
from app.core.models.user import User
from ..schemas.person_schema import PersonSchema
from ..schemas.rol_schema import RolSchema
from .user_mapper import UserMapperDTO
from .study_plan_mapper import StudyPlanMapperDTO
from fastapi.encoders import jsonable_encoder
from loguru import logger


class NodeMapperDTO:

    @staticmethod
    def DTOToCore(node_schema: NodeSchema) -> Node:
        logger.info('DTOToCore')
        node_dict = jsonable_encoder(node_schema)
        node = Node.from_dict(node_dict)
        return node

    @staticmethod
    def coreToDTO(node: Node) -> NodeSchema:
        logger.info('coreToDTO')
        node_schema = NodeSchema(
            id=node.id,
            id_neuralactions=node.id_neuralactions,
            node_type=node.node_type
        )
        return node_schema
