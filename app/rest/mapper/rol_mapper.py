from app.core.models.rol import Rol
from ..schemas.rol_schema import RolSchema
from loguru import logger


class RolMapperDTO:

    @staticmethod
    def DTOToCore(rolSchema: RolSchema) -> Rol:
        rol = Rol.from_dict(rolSchema)
        return rol

    @staticmethod
    def coreToDTO(rol: Rol) -> RolSchema:
        logger.info(rol.to_dict())
        rolSchema = RolSchema(
            id=rol.id,
            name=rol.name,
        )
        return rolSchema
