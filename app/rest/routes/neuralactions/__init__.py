from fastapi import APIRouter
from . import create_node

router = APIRouter(tags=["Neuralactions"])

router.include_router(create_node.router)
