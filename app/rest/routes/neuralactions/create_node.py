from typing import Any

from fastapi import APIRouter, HTTPException, Depends
from loguru import logger

from app.core.models.node import Node
from app.core.uses_cases.neuralactions.create_node_uc import CreateNodeUC
from app.data.implementation.neuralactions.create_node import CreateNodeImplementation
from app.rest.deps import is_admin_rol, reusable_oauth2
from app.rest.schemas.node_schema import NodeSchema
from app.rest.mapper.node_mapper import NodeMapperDTO

from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/api/v1/developer/auth/token')

@router.post("/", response_model=NodeSchema)
def create_node(*, req: NodeSchema, current_user = Depends(is_admin_rol), token= Depends(reusable_oauth2))-> Any:

    try:
        logger.info('create_node')

        node_data = NodeMapperDTO.DTOToCore(req)
        uc = CreateNodeUC(CreateNodeImplementation())
        node = uc.create(node_data)

        return NodeMapperDTO.coreToDTO(node)
    except Exception:
        logger.error('create_node: error to create node')
        raise HTTPException(
            status_code=400, detail="createNode: Error to create node"
        )
