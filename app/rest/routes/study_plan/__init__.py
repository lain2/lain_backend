from fastapi import APIRouter
from . import change_status_study_plan
from . import create_study_plan
from . import list_all_study_plans
from . import update_study_plan
from . import get_study_plan_rest

router = APIRouter(tags=["Study Plan"])

router.include_router(change_status_study_plan.router)
router.include_router(create_study_plan.router)
router.include_router(get_study_plan_rest.router)
router.include_router(list_all_study_plans.router)
router.include_router(update_study_plan.router)
