from typing import Any, List

from fastapi import APIRouter, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer
from app.core.uses_cases.study_plan.get_study_plan_uc import GetStudyPlanUC
from app.data.implementation.study_plan.get_study_plan_impl import GetStudyPlanImplementation
from app.rest.schemas.study_plan import GetStudyPlanScheme
from app.rest.mapper.study_plan_mapper import StudyPlanMapperDTO
from app.rest.deps import get_current_user, is_admin_rol
from app.core.exceptions.study_plan_exception import StudyPlanException
from loguru import logger
from app.rest.deps import get_current_user, is_admin_rol

router = APIRouter()


@router.get("/get", response_model=GetStudyPlanScheme)
def get_study_plan(*, key: str, value: str, current_user = Depends(is_admin_rol)) -> Any:
    try:
        logger.info('get_study_plan')
        uc = GetStudyPlanUC(GetStudyPlanImplementation())
        study_plan = uc.get_study_plan(key, value)
        studyPlanDTO = StudyPlanMapperDTO.coreToDTO(study_plan)
        return studyPlanDTO
    except StudyPlanException:
        logger.error(f'get_study_plan: get by {key} fail')
        raise HTTPException(status_code=400, detail=f'get_study_plan: get by {key} fail')


