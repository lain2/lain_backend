from typing import Any

from fastapi import APIRouter, HTTPException

from app.core.exceptions.study_plan_exception import StudyPlanException
from app.core.models.study_plan import StudyPlan
from app.core.uses_cases.study_plan.update_study_plan_uc import UpdateStudyPlanUC
from app.data.implementation.study_plan.update_study_plan import UpdateStudyPlanImplementation
# from app.rest.deps import get_token
from app.rest.neuralactions.models.workspace import Workspace
from app.rest.neuralactions.workspace.update_workspace import UpdateWorkSpace
from app.rest.schemas.study_plan import UpdateStudyPlanScheme

router = APIRouter()


@router.put("/", response_model=UpdateStudyPlanScheme)
def update_study_plan(*, req: UpdateStudyPlanScheme) -> Any:
    """Permite actualizar un plan de estudio."""
    try:
        update_in_neuralactions(req)
        study_plan = StudyPlan(req.id, req.code, req.name, req.description)
        uc = UpdateStudyPlanUC(UpdateStudyPlanImplementation())
        sp = uc.update(study_plan)
        return UpdateStudyPlanScheme(
            id=req.id, code=sp.code, name=sp.name, description=sp.description
        )

    except StudyPlanException:
        raise HTTPException(
            status_code=400, detail="CreateStudyPlanUC: Study Plan exist"
        )


def update_in_neuralactions(req: UpdateStudyPlanScheme) -> str:
    pass
    # try:
    #     name_neural = req.code + " - " + req.name
    #     workspace = Workspace(req.id, name_neural).to_dict()
    #     update = UpdateWorkSpace(get_token())
    #     updated = update.update(workspace=workspace)
    #     return updated.json()["id"] is not None
    # except HTTPException as error:
    #     raise error
