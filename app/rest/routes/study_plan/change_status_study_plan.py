from typing import Any

from fastapi import APIRouter, HTTPException

from app.core.exceptions.study_plan_exception import StudyPlanException
from app.core.uses_cases.study_plan.change_status_study_plan_uc import ChangeStatusStudyPlanUC
from app.data.implementation.study_plan.change_status_study_plan import ChangeStatusStudyPlanImplementation
from app.rest.schemas.study_plan import UpdateStatusStudyPlan
from loguru import logger

router = APIRouter()

@router.put("/{study_plan_id}", response_model=UpdateStatusStudyPlan, status_code=200)
def change_status_study_plan(*, study_plan_id: str) -> Any:
    """Permite actualizar el estado de un plan de estudio."""
    try:
        logger.info('change_status_study_plan')
        uc = ChangeStatusStudyPlanUC(ChangeStatusStudyPlanImplementation())
        sp = uc.change_status(study_plan_id)
        update = UpdateStatusStudyPlan(id=study_plan_id, updated=sp)
        return update

    except StudyPlanException:
        logger.info('Error to update status')
        raise HTTPException(
            status_code=400, detail="ChangeStatusStudyPlanUC: Error to update status"
        )
