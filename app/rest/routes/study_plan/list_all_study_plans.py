from typing import Any, List

from fastapi import APIRouter, HTTPException

from app.core.uses_cases.study_plan.list_all_study_plan_uc import ListAllStudyPlanUC
from app.data.implementation.study_plan.list_all_study_plan_impl import ListStudyPlanImplementation
from app.rest.mapper.study_plan_mapper import StudyPlanMapperDTO
from app.rest.schemas.study_plan import GetStudyPlanScheme

router = APIRouter()


@router.get("/", response_model=List[GetStudyPlanScheme])
def list_all_study_plans() -> Any:
    """Endpoint que busca todos los planes de estudio."""
    try:
        use_case = ListAllStudyPlanUC(ListStudyPlanImplementation())
        list_study_plans = use_case.get_all()
        resp = list(map(lambda x: StudyPlanMapperDTO.coreToDTO(x), list_study_plans))
        return resp
    except Exception:
        raise HTTPException(status_code=400, detail="list_all_study_plans: Error")
