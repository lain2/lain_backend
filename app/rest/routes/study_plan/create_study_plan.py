from typing import Any

from fastapi import APIRouter, HTTPException

from app.core.exceptions.study_plan_exception import StudyPlanException
from app.core.models.study_plan import StudyPlan
from app.core.uses_cases.study_plan.create_study_plan_uc import CreateStudyPlanUC
from app.data.implementation.study_plan.create_study_plan import CreateStudyPlanImplementation
from app.rest.neuralactions.models.workspace import Workspace
from app.rest.neuralactions.workspace.create_workspace import CreateWorkSpace
from app.rest.schemas.study_plan import CreateStudyPlanScheme, StudyPlanBase
from app.rest.service.neuralactions.create_workspace import create as create_neural
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/api/v1/developer/auth/token')

@router.post("/", response_model=StudyPlanBase)
def create_study_plan(*, req: CreateStudyPlanScheme) -> Any:
    """Endpoint para la creacion de un plan de estudio."""
    try:

        code = save_in_neuralactions(req)
        study_plan = StudyPlan(code, req.code, req.name, req.description)
        uc = CreateStudyPlanUC(CreateStudyPlanImplementation())
        sp = uc.save(study_plan)
        return CreateStudyPlanScheme(
            code=sp.code, name=sp.name, description=sp.description
        )
    except StudyPlanException:
        raise HTTPException(
            status_code=400, detail="CreateStudyPlanUC: Study Plan exist"
        )


def save_in_neuralactions(req: CreateStudyPlanScheme) -> str:
    try:
        name_neural = req.code + " - " + req.name
        workspace = Workspace(None, name_neural).to_dict()
        # create = CreateWorkSpace(get_token())
        # saved = create.save(workspace=workspace)
        saved = create_neural(workspace=workspace)
        return saved.json()["id"]
    except HTTPException as error:
        raise error
