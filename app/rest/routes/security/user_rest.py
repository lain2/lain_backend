import uuid
from typing import Any

from fastapi import APIRouter, HTTPException, Depends, Header
from fastapi.encoders import jsonable_encoder

from app.core.exceptions.user_exception import UserException
from app.core.models.user import User
from app.core.models.rol import Rol
from app.config.security import create_access_token
from app.core.uses_cases.security.create_user_uc import CreateUserUC
from app.core.uses_cases.security.get_user import GetUserUC
from app.data.implementation.security.create_user import CreateUserImplementation
from app.data.implementation.security.get_user import GetUserImplementation
from app.rest.schemas.user_schema import UserSchema, UserQuerySchema
from app.rest.mapper.user_mapper import UserMapperDTO
from loguru import logger
from app.rest.deps import get_current_user, is_admin_rol
from fastapi.security import OAuth2PasswordBearer

router = APIRouter()


@router.post("/", response_model=UserSchema)
def create_user(*, req: UserSchema, current_user = Depends(is_admin_rol)) -> Any:
    try:
        logger.info('create_user')
        user = UserMapperDTO.DTOToCore(req)
        uc = CreateUserUC(CreateUserImplementation())
        user = uc.save(user)
        logger.info(user.to_dict())
        userDTO = UserMapperDTO.coreToDTO(user)
        logger.info("Created successfully")
        return userDTO
    except UserException:
        logger.error("user_rest: Create fail")
        raise HTTPException(
            status_code=400, detail="user_rest: Create fail"
        )

@router.get("/", response_model=UserSchema)
def get_user(*, key: str, value: Any, current_user = Depends(is_admin_rol)) -> Any:
    try:
        logger.info('get_user')
        uc = GetUserUC(GetUserImplementation())
        user = uc.get_user(key, value)
        userDTO = UserMapperDTO.coreToDTO(user)
        return userDTO
    except UserException:
        logger.error("user_rest: Get fail")
        raise HTTPException(
            status_code=400, detail="user_rest: Create fail"
        )


