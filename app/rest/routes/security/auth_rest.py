from loguru import logger
from datetime import timedelta
from fastapi import APIRouter, HTTPException, Depends
from fastapi.encoders import jsonable_encoder
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from typing import Any

from app.config.config import settings
from app.core.models.user import User
from app.core.models.security import create_access_token
from app.core.exceptions.user_exception import UserException
from app.core.uses_cases.security.authenticate_uc import AuthenticateUC
from app.data.implementation.security.authenticate import AuthenticateImplementation
from app.rest.mapper.user_mapper import UserMapperDTO



oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/api/v1/developer/auth/token')
router = APIRouter()


@router.post("/token")
async def create_token(form_data: OAuth2PasswordRequestForm = Depends()) -> Any:
    try:
        logger.info('create_token')
        use_case = AuthenticateUC(AuthenticateImplementation())
        user = use_case.authenticate(form_data.username, form_data.password)
        time_expire = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
        token = create_access_token(user, time_expire)
        return token

    except UserException:
        logger.error('Incorrect email or password')
        raise HTTPException(status_code=400, detail="Incorrect email or password")
