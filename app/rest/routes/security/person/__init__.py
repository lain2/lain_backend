from fastapi import APIRouter
from . import person_create_rest

router = APIRouter(tags=["Person"])

router.include_router(person_create_rest.router)
