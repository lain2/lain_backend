import uuid
import json
from typing import Any

from fastapi import APIRouter, HTTPException, Depends, Request, Header

from app.core.exceptions.person_exception import PersonException
from app.core.models.user import User
from app.core.models.rol import Rol
from app.core.uses_cases.security.create_person_uc import CreatePersonUC
from app.data.implementation.security.create_person import CreatePersonImplementation
from app.rest.schemas.user_schema import UserSchema
from loguru import logger
from app.rest.deps import is_admin_rol, reusable_oauth2

from app.rest.schemas.person_schema import PersonSchema
from app.rest.mapper.person_mapper import PersonMapperDTO
from app.rest.service.service import get_user_by_id
from app.rest.service import get_study_plan_by_id
from fastapi.encoders import jsonable_encoder
from app.rest.service.neuralactions.create_node import create_in_neauralactions
from app.rest.service.node_client import post_node
from app.rest.service.neuralactions.tags import put_tag_node


router = APIRouter()


@router.post("/", response_model=PersonSchema)
def create_person(*, req: PersonSchema, current_user = Depends(is_admin_rol), token= Depends(reusable_oauth2))-> Any:
    try:

        logger.info('create_person_rest')

        req.user= get_user_by_id(current_user.id, token)
        req.study_plan = get_study_plan_by_id(req.study_plan_id, token)

        person = PersonMapperDTO.DTOToCore(req)
        uc = CreatePersonUC(CreatePersonImplementation())
        person = uc.save(person)
        personDTO = PersonMapperDTO.coreToDTO(person)

        data = mapToNeural(personDTO)
        logger.error(data)
        resp = create_in_neauralactions(data)

        node = {"id_neuralactions": resp['id'], "node_type": 'DOCENTE'}

        result = post_node(node, token)
        node_id = node['id_neuralactions']

        tag = {"id": "01f2097e-6c02-4403-80ce-8944b82bf479",
               "name": "docente",
               "hue": 350,
               "nodes": [node_id]}


        tag_data = put_tag_node(tag)
        logger.info(tag_data)

        return personDTO
    except Exception:
        logger.error("person_rest: Create person fail")
        raise HTTPException(
            status_code=400, detail="person_create_rest: Create person fail"
        )



def mapToNeural(personSchema: PersonSchema):
    logger.error('personNeural')

    try:

        # values = {
        #     "name": personSchema['firstname']+ " " + personSchema['lastname'],
        #     "imagen": personSchema['avatar'],
        #     "nombres": personSchema['firstname'],
        #     "apellidos": personSchema['lastname'],
        #     "email": personSchema['user']['email'],
        #     "cargo": personSchema['position'],
        #     "scheme_id": "2e00f4469071aec983637c73767b506e-182be0c5cdcd5072bb1864cdee4d3d6e"
        # }
        # workspace = { "active_workspace_id": "7b1126c077433c47c3ecf6d890a0a430-182be0c5cdcd5072bb1864cdee4d3d6e"}

        # logger.error(values)
        # logger.error(workspace)

        # personNeural = {
        #     "values": values,
        #     "workspace": workspace
        # }
        personNeural = json.dumps({
            "values" : {
                "name": personSchema['firstname']+ " " + personSchema['lastname'],
                "imagen": personSchema['avatar'],
                "nombres": personSchema['firstname'],
                "apellidos": personSchema['lastname'],
                "email": personSchema['user']['email'],
                "cargo": personSchema['position'],
                "scheme_id": "2e00f4469071aec983637c73767b506e-182be0c5cdcd5072bb1864cdee4d3d6e"
            },
            "workspaces": { "active_workspace_id": "7b1126c077433c47c3ecf6d890a0a430-182be0c5cdcd5072bb1864cdee4d3d6e"}
        })

        logger.error(personNeural)
        return personNeural
    except Exception as e:
        logger.error(e)
        logger.error('error :c')
