import json

import requests
from fastapi import HTTPException

from ..auth.token import generate_headers
from ..config import URL_WORKSPACES
from ..models.workspace import Workspace


class CreateWorkSpace:
    def __init__(self, token):
        self.token = token

    def save(self, workspace: Workspace):
        headers = generate_headers(self.token)
        exist = self.validate(headers, workspace["name"])
        if exist:
            raise HTTPException(
                status_code=400,
                detail="CreateStudyPlanUC: Study Plan exist in NeuralActions",
            )
        workspace = json.dumps(workspace)
        response = requests.post(URL_WORKSPACES, data=workspace, headers=headers)
        error = response.json()["error"]
        if error is not None:
            raise HTTPException(status_code=400, detail="CreateStudyPlanUC: " + error)

        return response

    def validate(self, headers, name: str):
        name = name.lower().strip()
        response = requests.get(URL_WORKSPACES, headers=headers)
        workspaces = response.json()["own"]
        names = [(w["name"]).lower().strip() for w in workspaces]
        return name in names
