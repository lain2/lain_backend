import json

import requests
from fastapi import HTTPException

from ..auth.token import generate_headers
from ..config import URL_WORKSPACES
from ..models.workspace import Workspace


class UpdateWorkSpace:
    def __init__(self, token):
        self.token = token

    def update(self, workspace: Workspace):
        headers = generate_headers(self.token)
        exist = self.validate_exist(headers, workspace["id"])
        if not exist:
            raise HTTPException(
                status_code=400,
                detail="UpdateStudyPlanNA: Study Plan id not exist in NeuralActions",
            )
        workspace = json.dumps(workspace)
        response = requests.put(URL_WORKSPACES, data=workspace, headers=headers)
        print(json.dumps(response.json()))
        if "error" in response:
            raise HTTPException(
                status_code=400, detail="UpdateStudyPlanNA: " + response["erro"]
            )

        return response

    def validate_exist(self, headers, id: str):
        response = requests.get(URL_WORKSPACES, headers=headers)
        workspaces = response.json()["own"]
        index = [(w["id"]) for w in workspaces]
        return id in index
