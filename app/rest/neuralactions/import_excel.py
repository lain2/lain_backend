import json

from lain_backend.neuralactions.users import login
from lain_backend.neuralactions.workspaces import get_all_workspaces


def generate_headers(data_json):
    return {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + data_json["access_token"],
    }


def print_json_pretty(obj_json):
    print(json.dumps(obj_json, indent=1))


data = {"name": "Desde python!!!"}
user = {"email": "aaron.ariperto@gmail.com", "password": "Ad46b36ff"}

login_data = login(user).json()

headers = generate_headers(login_data)

workspaces = get_all_workspaces(headers).json()
workspace = workspaces["own"][1]

print(workspace)
print_json_pretty(workspace.json())
