import requests

from lain_backend.neuralactions.config import URL_LOGIN


def login(user):
    """
    Return a response with data of login

    Paramters:
        user(dict): The dict with a user data

    Returns:
        request: Return a request object
    """
    response = requests.post(URL_LOGIN, data=user)
    return response
