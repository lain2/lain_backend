import requests

from app.rest.neuralactions.config import URL_LOGIN

# 90 minutes in seconds. Setting this a little lower would
# probably be better to account for network latency.
MAX_AGE = 90 * 60
# /tmp/libname/ needs to exist for this to work; creating it
# if necessary shouldn't give you much trouble.


def generate_headers(token):
    return {"Content-Type": "application/json", "Authorization": "Bearer " + token}


def login(user):
    """
    Return a response with data of login

    Paramters:
        user(dict): The dict with a user data

    Returns:
        request: Return a request object
    """
    response = requests.post(URL_LOGIN, data=user)
    return response


def get_paypal_token():

    # if os.environ['TOKEN_NEURAL']:
    # return os.environ['TOKEN_NEURAL']
    # else:
    user = {"email": "himuraxkenji@gmail.com", "password": "Ad46b36ff"}
    token = login(user).json()["access_token"]
    print("get_paypal_token")
    print(token)
    return token


# import requests

# from app.neuralactions.config import URL_LOGIN

# def login(user):
#     """
#     Return a response with data of login

#     Paramters:
#         user(dict): The dict with a user data

#     Returns:
#         request: Return a request object
#     """
#     response = requests.post(URL_LOGIN, data=user)
#     return response
