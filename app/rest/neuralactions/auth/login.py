import requests

from ..config import URL_LOGIN


def generate_headers(token):
    return {"Content-Type": "application/json", "Authorization": "Bearer " + token}


def login(user):
    """
    Return a response with data of login

    Paramters:
        user(dict): The dict with a user data

    Returns:
        request: Return a request object
    """
    response = requests.post(URL_LOGIN, data=user)
    return response
