import requests

from lain_backend.neuralactions.config import URL_WORKSPACES


def get_all_workspaces(headers):
    response = requests.get(URL_WORKSPACES, headers=headers)
    return response


def post_workspace(headers, workspace):
    response = requests.post(URL_WORKSPACES, data=workspace, headers=headers)
    return response
