import json

from app.rest.schemas.study_plan import CreateStudyPlanScheme


class Workspace:
    def __init__(self, id: str = None, name: str = None):
        self.id = id
        self.name = name

    def convert(self) -> dict:
        return json.dumps(
            {
                "id": self.id,
                "name": self.name,
            }
        )

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "name": self.name,
        }
