import requests

from lain_backend.neuralactions.config import URL_SCHEMA, URL_SCHEME


def get_schemes_workspace(headers, id_workspace):
    response = requests.get(URL_SCHEMA + id_workspace, headers=headers)
    return response


def get_scheme_by_id(headers, id_schema):
    response = requests.get(URL_SCHEME + id_schema, headers=headers)
    return response
