from typing import List

from app.data.db.session import SessionLocal, engine
from app.rest.api import api_router
from fastapi import Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session
from app.config.config import settings
# app = FastAPI(
#     title="Lain", openapi_url=f"/api/v1/openapi.json"
# )
# Dependency
# def get_db() -> Session:
#     db = SessionLocal()
#     try:
#         print('intento')
#         yield db
#     finally:
#         print('finally')
#         db.close()






def get_app():
    origins = [
        "http://localhost",
        "http://localhost:3000",
        "http://localhost:3000/",
    ]
    app = FastAPI(title=settings.SERVER_NAME, openapi_url=f"/api/v1/openapi.json")
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.include_router(api_router, prefix=settings.ROUTE)
    return app
