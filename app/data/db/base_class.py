import datetime
import re
from typing import Any

from sqlalchemy import Boolean, Column, DateTime, String
from sqlalchemy.ext.declarative import as_declarative, declared_attr


def camelCaseToSnake(name: str):
    """Convierte el nombre de las clases en formato db."""
    return re.sub(r"(?<!^)(?=[A-Z])", "_", name).lower()


@as_declarative()
class Base:
    """Permite la generacion de la tabla en la db mediante alembic."""

    id: Any
    __name__: str
    # Generate __tablename__ automatically
    created_at = Column(DateTime, default=datetime.datetime.now)
    updated_at = Column(DateTime, onupdate=datetime.datetime.now)
    created_by = Column(String, default='')
    updated_by = Column(String, default='')
    status = Column(Boolean, default=True)

    @declared_attr
    def __tablename__(cls) -> str:
        name = camelCaseToSnake(cls.__name__).replace("_entity", "")
        return name
