from app.data.db.base_class import Base
from app.data.entities.study_plan_entity import StudyPlanEntity
from app.data.entities.token_neural_entity import TokenNeuralEntity
from app.data.entities.rol_entity import RolEntity
from app.data.entities.user_entity import UserEntity
from app.data.entities.person_entity import PersonEntity
from app.data.entities.node_entity import NodeEntity

