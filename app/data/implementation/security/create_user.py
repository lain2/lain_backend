import uuid
from typing import Optional

from app.core.models.user import User
from app.core.repositories.security.repo_create_user import RepoCreateUser
from app.data import deps
from app.data.crud.crud_user import crud_user
from app.data.mappers.user_mapper import UserMapper
from loguru import logger


class CreateUserImplementation(RepoCreateUser):

    def save(self, user: User) -> User:
        logger.info('save')
        db = next(deps.get_db(), None)  # type: ignore
        user_entity = UserMapper.modelToData(user=user)
        user_entity = crud_user.create(db=db, user=user_entity)
        user = UserMapper.dataToModel(user_entity=user_entity)
        return user

    def get_by_email(self, email: str) -> Optional[User]:
        logger.info('get_by_email')
        db = next(deps.get_db(), None)  # type: ignore
        user_entity = crud_user.get_by_email(db=db, email=email)
        if user_entity is None:
            return None
        return UserMapper.dataToModel(user_entity=user_entity)
