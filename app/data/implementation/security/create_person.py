import uuid
from typing import Optional

from app.core.models.person import Person
from app.core.repositories.security.repo_create_person import RepoCreatePerson
from app.data import deps
from app.data.crud.crud_person import crud_person
from app.data.mappers.person_mapper import PersonMapper
from loguru import logger
from fastapi.encoders import jsonable_encoder


class CreatePersonImplementation(RepoCreatePerson):

    def save(self, person: Person) -> Person:
        logger.info('save')
        logger.error('save')
        db = next(deps.get_db(), None)  # type: ignore
        person_entity = PersonMapper.modelToData(person=person)
        person_entity = crud_person.create(db=db, obj_in=person_entity)
        person = PersonMapper.dataToModel(person_entity=person_entity)
        return person

    def get_by_user_id(self, user_id: str) -> Optional[Person]:
        logger.info('get_by_user_id')
        db = next(deps.get_db(), None)  # type: ignore
        person_entity = crud_person.get_by_user_id(db=db, user_id=user_id)
        logger.error(jsonable_encoder(person_entity))
        if person_entity is None:
            return None
        return PersonMapper.dataToModel(person_entity=person_entity)

