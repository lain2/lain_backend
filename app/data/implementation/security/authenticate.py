import uuid
from typing import Optional
from loguru import logger

from app.core.models.user import User
from app.core.models.security import verify_password
from app.core.repositories.security.repo_authenticate import RepoAuthenticate
from app.data import deps
from app.data.crud.crud_user import crud_user
from app.data.mappers.user_mapper import UserMapper


class AuthenticateImplementation(RepoAuthenticate):

    def get_user_by_username(self, username:  str) ->  User:
        logger.info('get_user_by_username')
        db = next(deps.get_db(), None)  # type: ignore
        user_entity = crud_user.get_by_username(db=db, username=username)
        if user_entity is None:
            return None
        return UserMapper.dataToModel(user_entity=user_entity)

    def verify_password(self, user: User, password: str) -> bool:
        logger.info('verify_password')
        db = next(deps.get_db(), None)  # type: ignore
        logger.info(user.password)
        logger.info(password)
        valid_password = verify_password(password, user.password)

        return valid_password

