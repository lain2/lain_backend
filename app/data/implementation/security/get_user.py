import uuid
from typing import Optional, Any

from app.core.models.user import User
from app.core.repositories.security.repo_get_user import RepoGetUser
from app.data import deps
from app.data.crud.crud_user import crud_user
from app.data.mappers.user_mapper import UserMapper
from loguru import logger
from fastapi.encoders import jsonable_encoder


class GetUserImplementation(RepoGetUser):


    def get_user_by_id(self, id: str) -> User:
        logger.info('get_user_by_id')
        db = next(deps.get_db(), None)  # type: ignore
        user_entity = crud_user.get(db, id)
        if user_entity is None:
            return None
        user = UserMapper.dataToModel(user_entity=user_entity)
        return user

    def get_user_by_username(self, username: str) -> User:
        logger.info('get_user_by_username')
        db = next(deps.get_db(), None)  # type: ignore
        user_entity = crud_user.get_by_username(db=db, username=username)
        if user_entity is None:
            return None
        user = UserMapper.dataToModel(user_entity=user_entity)
        logger.info(jsonable_encoder(user))
        return user

    def get_user_by_email(self, email: str) -> User:
        logger.info('get_user_by_email')
        db = next(deps.get_db(), None)  # type: ignore
        user_entity = crud_user.get_by_email(db=db, email=email)
        if user_entity is None:
            return None
        user = UserMapper.dataToModel(user_entity=user_entity)
        logger.info(jsonable_encoder(user))
        return user


    def validate_user(self, user_entity: Any) ->User:
        logger.info('validate_user')
        if user_entity is None:
            return None
        user = UserMapper.dataToModel(user_entity=user_entity)
        logger.info(jsonable_encoder(user))
        return user

