from typing import List

from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_get_study_plan import RepoGetStudyPlan
from app.data import deps
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.mappers.study_plan_mapper import StudyPlanMapper


class GetStudyPlanImplementation(RepoGetStudyPlan):

    def get_study_plan_by_id(self, id: str) -> StudyPlan:
        db = next(deps.get_db(), None)
        study_plan_entity = crud_study_plan.get(db=db, id=id)
        study_plan = StudyPlanMapper.dataToModel(study_plan_entity)
        return study_plan

    def get_study_plan_by_code(self, code: str) -> StudyPlan:
        db = next(deps.get_db(), None)
        study_plan_entity = crud_study_plan.get_by_code(db=db, code=code)
        study_plan = StudyPlanMapper.dataToModel(study_plan_entity)
        return study_plan

