from typing import Optional

from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_update_study_plan import RepoUpdateStudyPlan
from app.data import deps
from app.data.crud.crud_study_plan import crud_study_plan


class UpdateStudyPlanImplementation(RepoUpdateStudyPlan):
    """Implementacion de modificar plan de estudio."""

    def update(self, study_plan: StudyPlan) -> StudyPlan:
        """Actualizar en la base de datos el plan de estudio."""
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.update(db=db, study_plan=study_plan)
        sp = StudyPlan(data.id, data.code, data.name, data.description)
        return sp

    def get_by_code(self, code: str) -> Optional[StudyPlan]:
        """Busca un plan de estudio por el codigo."""
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.get_by_code(db=db, code=code)
        if data is None:
            return None

        return StudyPlan(data.id, data.code, data.name, data.description)
