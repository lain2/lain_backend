from typing import List

from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_list_all_study_plan import RepoListAllStudyPlan
from app.data import deps
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.mappers.study_plan_mapper import StudyPlanMapper


class ListStudyPlanImplementation(RepoListAllStudyPlan):
    """Implementacion del repositorio del core."""

    def get_all(self) -> List[StudyPlan]:
        """Buscar en la db todos los planes de estudio existentes."""
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.get_all(db=db)
        study_plans = list(map(lambda d: StudyPlanMapper.dataToModel(d), data))
        return study_plans
