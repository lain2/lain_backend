from typing import Optional

from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_create_study_plan import RepoCreateStudyPlan
from app.data import deps
from app.data.crud.crud_study_plan import crud_study_plan


class CreateStudyPlanImplementation(RepoCreateStudyPlan):
    """Implementacion del repositorio del core."""

    def save(self, study_plan: StudyPlan) -> StudyPlan:
        """Guarda en la base de datos el plan de estudio."""
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.create(db=db, study_plan=study_plan)
        sp = StudyPlan(data.id, data.code, data.name, data.description)
        return sp

    def get_by_code(self, code: str) -> Optional[StudyPlan]:
        """Busca si existe un plan de estudio repetido."""
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.get_by_code(db, code)
        if data is None:
            return None

        return StudyPlan(data.id, data.code, data.name, data.description)
