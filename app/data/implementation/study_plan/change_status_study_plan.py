from typing import Optional

from app.core.models.study_plan import StudyPlan
from app.core.repositories.study_plan.repo_change_status_study_plan import RepoChangeStatusStudyPlan
from app.data import deps
from app.data.crud.crud_study_plan import crud_study_plan


class ChangeStatusStudyPlanImplementation(RepoChangeStatusStudyPlan):
    def get_by_id(self, id: str) -> Optional[StudyPlan]:
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.get(db, id)
        if data is None:
            return None
        study_plan = StudyPlan(id, data.code, data.name, data.description)
        study_plan.status = data.status
        return study_plan

    def update_status(self, study_plan: StudyPlan) -> bool:
        db = next(deps.get_db(), None)  # type: ignore
        data = crud_study_plan.update_status(db, study_plan)
        return data is not None
