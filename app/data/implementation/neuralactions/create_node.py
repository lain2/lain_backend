import uuid
from typing import Optional

from app.core.models.node import Node
from app.core.repositories.neuralactions.repo_create_node import RepoCreateNode
from app.data import deps
from app.data.crud.crud_node import crud_node

from app.data.mappers.node_mapper import NodeMapper
from loguru import logger
from fastapi.encoders import jsonable_encoder


class CreateNodeImplementation(RepoCreateNode):

    def save(self, node: Node) -> Node:
        logger.info('save')
        db = next(deps.get_db(), None)  # type: ignore
        node_entity = NodeMapper.modelToData(node=node)
        node_entity = crud_node.create(db=db, obj_in=node_entity)
        node = NodeMapper.dataToModel(node_entity=node_entity)
        return node

