from sqlalchemy.orm import Session

from app.data.db.session import SessionLocal


def get_db() -> Session:  # type: ignore
    """Genera la session para la base de datos."""
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()
