from sqlalchemy.orm import Session

from app.data.crud.base import CRUDBase
from app.data.entities.token_neural_entity import TokenNeuralEntity


class UpdateToken(CRUDBase[TokenNeuralEntity]):
    """Implementacion de base de datos."""

    def update(self, db: Session, *, token: TokenNeuralEntity) -> TokenNeuralEntity:
        """Permite actualizar un token de neuralactions."""
        token_in_db = (
            db.query(TokenNeuralEntity).filter(TokenNeuralEntity.id == 1).first()
        )
        print("TOken")
        print(token_in_db.token)
        print("update")
        print(token.token)
        token_in_db.token = token.token
        db.add(token_in_db)
        db.commit()
        db.refresh(token_in_db)
        return token_in_db

    def get_token(self, db: Session) -> TokenNeuralEntity:
        """Busca el token en la base de datos."""
        data = db.query(TokenNeuralEntity).filter(TokenNeuralEntity.id == 1).first()
        print("get_token data: ")
        print(data)
        return data


update_token = UpdateToken(TokenNeuralEntity)
