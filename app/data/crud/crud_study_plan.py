from typing import Optional, Union, Dict, Any, List
from sqlalchemy.orm import Session

from app.data.crud.crud_base import CRUDBase
from app.data.entities.study_plan_entity import StudyPlanEntity
from loguru import logger
from fastapi.encoders import jsonable_encoder
from app.core.models.study_plan import StudyPlan


class CRUDStudyPlan(CRUDBase[StudyPlanEntity]):

    def create(self, db: Session, *, study_plan: StudyPlan) -> StudyPlanEntity:
        db_obj = StudyPlanEntity(
            id=study_plan.id,
            code=study_plan.code,
            name=study_plan.name,
            description=study_plan.description,
        )  # type: ignore
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_by_code(self, db: Session, *, code: str) -> Optional[StudyPlanEntity]:
        return db.query(StudyPlanEntity).filter(StudyPlanEntity.code == code).first()

    def get_all(self, db: Session) -> List[StudyPlanEntity]:
        data = db.query(StudyPlanEntity).all()
        return data

    def update_status(self, db: Session, study_plan: StudyPlan) -> StudyPlanEntity:
        obj_db = db.query(StudyPlanEntity).get(study_plan.id)
        obj_db.status = study_plan.status

        db.commit()
        return obj_db

    def update(self, db: Session, study_plan: StudyPlan) -> StudyPlanEntity:
        data = StudyPlanEntity(
            id=study_plan.id,
            code=study_plan.code,
            name=study_plan.name,
            description=study_plan.description,
        )  # type: ignore

        obj_db = db.query(StudyPlanEntity).get(study_plan.id)

        obj_db.id = data.id
        obj_db.code = data.code
        obj_db.name = data.name
        obj_db.description = data.description

        data.status = obj_db.status
        db.commit()
        return data


crud_study_plan = CRUDStudyPlan(StudyPlanEntity)
