from typing import Optional, Union, Dict, Any
from sqlalchemy.orm import Session
from loguru import logger

from app.config.security import get_password_hash, verify_password
from app.data.crud.crud_base import CRUDBase
from app.data.entities.user_entity import UserEntity
from fastapi.encoders import jsonable_encoder


class CRUDUser(CRUDBase[UserEntity]):

    def get_by_email(self, db: Session, *, email: str) -> Optional[UserEntity]:
        return db.query(UserEntity).filter(UserEntity.email == email).first()

    def get_by_username(self, db: Session, *, username: str) -> Optional[UserEntity]:
        return db.query(UserEntity).filter(UserEntity.username == username).first()

    def create(self, db: Session, *, user: UserEntity) -> UserEntity:
        db_obj = UserEntity(
            id = user.id,
            username = user.username,
            email = user.email,
            password = get_password_hash(user.password),
            avatar = user.avatar,
            google=user.google,
            rol_id = user.rol_id
        )  # type: ignore
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(
        self, db: Session, *, db_obj: UserEntity, obj_in: Union[UserEntity, Dict[str, Any]]
    ) -> UserEntity:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if update_data["password"]:
            hashed_password = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password
        return super().update(db, db_obj=db_obj, obj_in=update_data)


    def authenticate(self, db: Session, *, email: str, password: str) -> Optional[UserEntity]:
        user = self.get_by_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.hashed_password):
            return None
        return user

    # def is_active(self, user: User) -> bool:
    #     return user.is_active

    # def is_superuser(self, user: User) -> bool:
    #     return user.is_superuser


crud_user = CRUDUser(UserEntity)
