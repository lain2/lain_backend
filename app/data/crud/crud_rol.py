from typing import Optional
from sqlalchemy.orm import Session

from app.data.crud.crud_base import CRUDBase
from app.data.entities.rol_entity import RolEntity


class CRUDRol(CRUDBase[RolEntity]):


    def get_by_name(self, db: Session, *, name: str) -> Optional[RolEntity]:
        return db.query(RolEntity).filter(RolEntity.name == name).first()


crud_rol = CRUDRol(RolEntity)
