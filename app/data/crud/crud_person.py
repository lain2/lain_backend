from typing import Optional, Union, Dict, Any
from sqlalchemy.orm import Session

from app.data.crud.crud_base import CRUDBase
from app.data.entities.person_entity import PersonEntity
from loguru import logger
from fastapi.encoders import jsonable_encoder


class CRUDPerson(CRUDBase[PersonEntity]):


    def get_by_user_id(self, db: Session, *, user_id: str) -> Optional[PersonEntity]:
        return db.query(PersonEntity).filter(PersonEntity.user_id == user_id).first()


crud_person = CRUDPerson(PersonEntity)
