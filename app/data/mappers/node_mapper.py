from loguru import logger

from app.core.models.node import Node
from app.data import deps
from app.data.crud.crud_node import crud_node
from app.data.entities.node_entity import NodeEntity

from fastapi.encoders import jsonable_encoder


class NodeMapper:

    @staticmethod
    def modelToData(node: Node) -> NodeEntity:
        logger.info('modelToData')
        node_entity = NodeEntity()
        node_entity.id = node.id
        node_entity.id_neuralactions = node.id_neuralactions
        node_entity.node_type = node.node_type
        node_entity.status = node.status

        return node_entity


    @staticmethod
    def dataToModel(node_entity: NodeEntity) -> Node:
        logger.info('dataToModel')
        db = next(deps.get_db(), None)  # type: ignore
        node = Node(
            node_entity.id,
            node_entity.id_neuralactions,
            node_entity.node_type
        )
        node.status = node_entity.status

        return node
