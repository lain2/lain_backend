from loguru import logger

from app.core.models.person import Person
from app.data import deps
from app.data.crud.crud_user import crud_user
from app.data.crud.crud_study_plan import crud_study_plan
from app.data.entities.person_entity import PersonEntity

from app.core.models.user import User
from app.core.models.study_plan import StudyPlan
from app.data.entities.user_entity import UserEntity
from fastapi.encoders import jsonable_encoder
from .user_mapper import UserMapper
from .study_plan_mapper import StudyPlanMapper


class PersonMapper:

    @staticmethod
    def modelToData(person: Person) -> PersonEntity:
        logger.info('modelToData')
        person_entity = PersonEntity()
        person_entity.id = person.id
        person_entity.lastname = person.lastname
        person_entity.firstname = person.firstname
        person_entity.position = person.position
        person_entity.avatar = person.avatar
        person_entity.user_id = person.user.id
        person_entity.study_plan_id = person.study_plan.id

        return person_entity


    @staticmethod
    def dataToModel(person_entity: PersonEntity) -> Person:
        logger.info('dataToModel')
        db = next(deps.get_db(), None)  # type: ignore
        user = crud_user.get(db=db, id=person_entity.user_id)
        user_data = UserMapper.dataToModel(user)
        user = User.from_dict(jsonable_encoder(user_data))
        logger.warning("======STP=======")

        study_plan = crud_study_plan.get(db=db, id=person_entity.study_plan_id)
        study_plan_data = StudyPlanMapper.dataToModel(study_plan)
        study_plan = StudyPlan.from_dict(jsonable_encoder(study_plan_data))

        logger.warning(study_plan)
        logger.error('pase')


        person = Person(
            person_entity.id,
            person_entity.lastname,
            person_entity.firstname,
            person_entity.position,
            person_entity.avatar,
            user,
            study_plan
        )
        person.status = person_entity.status

        logger.warning(jsonable_encoder(person))
        return person
