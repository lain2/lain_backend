from app.core.models.study_plan import StudyPlan
from app.data.entities.study_plan_entity import StudyPlanEntity


class StudyPlanMapper:
    """Realiza el mapeo de un tipo de objeto a otro."""

    @staticmethod
    def modelToData(self, study_plan: StudyPlan) -> StudyPlanEntity:
        study_plan_entity = StudyPlanEntity()
        study_plan_entity.id = study_plan.id
        study_plan_entity.code = study_plan.code
        study_plan_entity.name = study_plan.name
        study_plan_entity.description = study_plan.description
        study_plan_entity.status = study_plan.status

        return study_plan_entity

    @staticmethod
    def dataToModel(study_plan_entity: StudyPlanEntity) -> StudyPlan:
        study_plan = StudyPlan(
            study_plan_entity.id,
            study_plan_entity.code,
            study_plan_entity.name,
            study_plan_entity.description,
        )
        study_plan.status = study_plan_entity.status

        return study_plan
