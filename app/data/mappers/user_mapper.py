from app.data import deps
from app.core.models.user import User
from app.core.models.rol import Rol
from app.data.entities.user_entity import UserEntity
from fastapi.encoders import jsonable_encoder
from app.data.crud.crud_rol import crud_rol
from sqlalchemy.orm import mapper
from loguru import logger


class UserMapper:

    @staticmethod
    def modelToData(user: User) -> UserEntity:
        logger.info('modelToData')
        logger.info(jsonable_encoder(user))
        logger.info(type(user))
        logger.info(type(user.rol))
        user_entity = UserEntity()
        user_entity.id = user.id
        user_entity.username = user.username
        user_entity.email = user.email
        user_entity.password = user.password
        user_entity.avatar = user.avatar
        user_entity.rol_id = user.rol.id
        user_entity.google = user.google
        user_entity.status = user.status

        return user_entity

    @staticmethod
    def dataToModel(user_entity: UserEntity) -> User:
        logger.info('dataToModel')
        db = next(deps.get_db(), None)  # type: ignore
        rol_entity = crud_rol.get(db=db, id=user_entity.rol_id)
        rol_entity = jsonable_encoder(rol_entity)
        rol = Rol.from_dict(rol_entity)


        user= User(
            user_entity.id,
            user_entity.username,
            user_entity.email,
            user_entity.password,
            user_entity.avatar,
            user_entity.google,
            rol,
        )
        user.status = user_entity.status

        return user
