from sqlalchemy import Column, String, Boolean, ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship

from ..db.base_class import Base


class NodeEntity(Base):

    id = Column(String, primary_key=True, index=True)
    id_neuralactions = Column(String, index=True)
    node_type = Column(String, index=True)
