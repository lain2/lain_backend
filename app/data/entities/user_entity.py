from sqlalchemy import Column, String, Boolean, ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship

from ..db.base_class import Base


class UserEntity(Base):

    id = Column(String, primary_key=True, index=True)
    username = Column(String, index=True)
    email= Column(String, index=True)
    password= Column(String, index=True)
    avatar= Column(String, index=True)
    google= Column(Boolean, index=True)
    rol_id = Column(String, ForeignKey("rol.id"))
    rol = relationship("RolEntity", back_populates="user")
    person = relationship("PersonEntity", back_populates="user")
