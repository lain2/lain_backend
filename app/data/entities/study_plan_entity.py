from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from ..db.base_class import Base


class StudyPlanEntity(Base):
    """Representa entidad en la base de datos."""

    id = Column(String, primary_key=True, index=True)
    code = Column(String, index=True)
    name = Column(String, index=True)
    description = Column(String, index=True)
    person = relationship("PersonEntity", back_populates="study_plan")
