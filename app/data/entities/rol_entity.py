from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from ..db.base_class import Base


class RolEntity(Base):

    id = Column(String, primary_key=True, index=True)
    name = Column(String, index=True)
    user = relationship("UserEntity", cascade="all, delete-orphan", back_populates="rol")
