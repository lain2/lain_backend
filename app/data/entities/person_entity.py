from sqlalchemy import Column, String, Boolean, ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship

from ..db.base_class import Base


class PersonEntity(Base):

    id = Column(String, primary_key=True, index=True)
    lastname= Column(String, index=True)
    firstname= Column(String, index=True)
    position= Column(String, index=True)
    avatar= Column(String, index=True)
    user_id = Column(String, ForeignKey("user.id"))
    user = relationship("UserEntity", back_populates="person")
    study_plan_id = Column(String, ForeignKey("study_plan.id"))
    study_plan = relationship("StudyPlanEntity", back_populates="person")
