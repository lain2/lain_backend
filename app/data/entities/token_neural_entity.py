from sqlalchemy import Column, Integer, String

from ..db.base_class import Base


class TokenNeuralEntity(Base):
    """Representa entidad en la base de datos."""

    id = Column(Integer, primary_key=True, index=True)
    token = Column(String, index=True)
