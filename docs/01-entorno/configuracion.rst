.. highlight:: shell

==================
Entorno de trabajo
==================

Para poder realizar el trabajo se tuvo que primero realizar toda la configuracion del entorno de trabajo.
Aunque parece una tarea realtivamente sencilla, si desde un comienzo el proyecto se no tiene una estuctura
bien definida y un conjunto de herramientas puestas a punto, puede que en el futuro pueda tener problemas
que retrasen el proyecto. Por lo que en esta seccion veremos se realizo la configuracion del backend y frontend
del proyecto.

Cada herramienta que se mostrara a continuacion implico el estudio de diferentes puntos importantes para que
el proyecto pueda escalar de segura. Gracias a esta disciplina posteriomente los desarrolladores solamente requeriran
clonar nuestro proyecto para realizar el desarrollo.


Contenido:

.. toctree::
    :maxdepth: 1

    backend/index
    frontend/index
