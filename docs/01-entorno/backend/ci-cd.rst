Pipeline CI/CD
==============


Posteriormente se creó en gitlab una issue mediante los tableros  de gitlab donde se especifica que se realizará la configuración del pipeline para que cada vez que realicemos un commit se ejecuten las siguientes tareas:

Instalación de dependencias
Análisis estático
Unit test
Documentación

	Para realizar esto debemos crear en la raíz del proyecto un archivo llamado “.gitlab-ci.yml” en la raíz del proyecto. En este archivo debemos agregar el siguiente código:

	image: "python:3.8.5"

before_script:
   - pip install -r requirements/dev.txt

stages:
   - Static analyze
   - Unit test

Static analyze:
   stage: Static analyze
   script:
       - make lint
       - make format

Unit test:
   stage: Unit test
   dependencies:
       - Static analyze
   script:
       - python -m pytest -svv

A continuación se explica el archivo línea a línea.

En la primera línea image: "python:3.8.5" indicamos que imagen de Docker debe tomar gitlab desde un repositorio de imágenes prearmadas. En este caso sera una imagen de docker con python 3.8.5

En el segundo bloque donde figura el código
before_script:
   - pip install -r requirements/dev.txt
se indica que se deben instalar las dependencias que se encuentran en en archivo dev.txt antes de realizar cualquier acción del pipeline.

El tercer bloque
stages:
   - Static analyze
   - Unit test
indica las etapas que tendrá nuestro pipeline.

El cuarto bloque

Static analyze:
   stage: Static analyze
   script:
       - make lint
       - make format
contiene todo lo referente a la etapa de analisis. Como se puede observar, se ejecutara el comando lint y el comando format. Estos pertenecen a al archivo makefile que se encuentra tambien en la raiz del proyecto. Dentro del make file se debe agregar

lint: ## check style with flake8
  flake8 lain_backend tests

format: ## reformat style with black
  black lain_backend tests


el primer script llamado lint lo que hace es ejecutar un linter, sobre el código para verificar que se respeta el estándar de codificación llamado PEP-8. Por otra parte el comando format, ejecuta black, que es un formater que modifica el código a formato PEP-8.

