Backend
=======

El backend se decidio que se usaria python 3.8, esto es debido a que permite a emplear modelos de machine learning de
forma sencilla mediante el uso de diversas librerias.

Para que el proyecto pueda escalar, se implementara Clean Architecture, lo que implicara el uso de librerias que
permitan realizar testing y definir una estructura de directorios.

Por ultimo el proyecto sera alojado en un repositorio de gitlab, con lo cual se podra usar herramientas de devops para
poder organizar el proyecto, tener un flujo de trabajo integrado y realizar operaciones de CI/CD. Dado que el codigo
sera de dominio publico, se seguira el estandar de calidad para la codificacion del mismo llamado PEP-8, por lo que se
configurara un linter y el formater para que no se suba codigo que no respete dicho estandar. Y en a la hora de
realizar commits tambien se respetara el estandar de conventional commits.


Contenido

.. toctree::
    :maxdepth: 1
    :includehidden:

    herramientas
    ci-cd
