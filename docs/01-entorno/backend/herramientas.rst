Herramientas
============

Venv
````

Es una herramienta diseñada para poder desarrollar proyectos en Python trabajando con diferentes versiones de paquetes
que, de otra forma, entrarían en conflicto. Por ejemplo, si un programador quiere desarrollar dos proyectos con diferentes
versiones de Django, no es posible tener las dos versiones instaladas en el directorio de bibliotecas de Python del
sistema, a no ser que se utilice una herramienta como virtualenv. Con virtualenv, el programador crearía dos entornos
aislados, e instalaría cada versión de Django en uno de los entornos.

Para realizar la instalacion se emplea el siguiente comando:

.. code-block:: console

    $ pip3 install virtualenv

Luego para crear el entorno virtual para el proyecto y activarlo se emplean los siguientes comandos:

.. code-block:: console

    # lain-env es el nombre del entorno
    $ python3 -m venv lain-env
    $ source lain-env/bin/active

Cookiecutter
````````````

Es una utilidad de linea de comandos que permite crear proyectos a partir de plantillas prediseñadas o tambien de la
comunidad. Para el proyecto emplearemos la plantilla creada por `Leonardo Giordani`_. La eleccion de este template es
debido a que posee integrada varias librerias, entre ellas se encuentran:

* **Pytest**: permite test unitarios con pytest
* **Travis-CI**: para testing con integración continua
* **Sphinx docs**: Generación de documentación automática.
* **Punch**: Un actualizador de versiones.

Otra ventaja que provee esta plantilla, es que los scripts de ejecucion del proyecto y sus herramientas, se encuentran
configurados en un makefile. Por lo que si requerimos incluir alguna herramienta nueva, debemos crear los comandos
de ejecucion en dicho fichero.

Para la instalación se y creación del proyecto se usaron los siguientes comandos:

.. code-block:: console

    # Estas operaciones se realizan con el enviroment que creamos previamente
    $ pip install -U cookiecutter
    $ cookiecutter https://github.com/lgiordani/cookiecutter-pypackage

Al ejecutar el comando de creación de proyecto se realizarán una serie de preguntas para configurar el proyecto, a
continuación se detallan que se colocó para el proyecto.

.. code-block:: console

    You've downloaded /home/deathslayer/.cookiecutters/cookiecutter-pypackage before. Is it okay to delete and
    re-download it? [yes]: y
    Full_name [Audrey Roy Greenfeld]: Riperto Adriel Aaron
    mail [aroy@alum.mit.edu]: aaron.ariperto@gmail.com
    github_username [audreyr]: himuraxkenji
    project_name [Python Boilerplate]: lain_backend
    project_slug [lain_backend]: lain_backend
    project_short_description [Python Boilerplate contains all the boilerplate you need to create a Python package.]: [enter]
    pypi_username [himuraxkenji]: [enter]
    version [0.1.0]: [enter]
    use_pytest [n]: y
    use_pypi_deployment_with_travis [y]: [enter]
    Select command_line_interface:
    1 - Click
    2 - No command-line interface
    Choose from 1, 2 [1]: 1
    create_author_file [y]: [enter]
    Select open_source_license:
    1 - MIT license
    2 - BSD license
    3 - ISC license
    4 - Apache Software License 2.0
    5 - GNU General Public License v3
    6 - Not open source
    Choose from 1, 2, 3, 4, 5, 6 [1]: [enter]

Con esto ya tendremos el proyecto base creado, pero las dependencias del mismo no se encuentran instaladas. El proyecto
cuenta con 3 archivos de dependencias alojados en la carpeta requirements de la raiz del proyecto. Estos archivos
debemos ejecutarlos para poder tener las dependencias instaladas de forma local.


.. code-block:: console

    $ pip install -r requirements/dev.txt
    $ pip install -r requirements/prod.txt
    $ pip install -r requirements/test.txt

.. _Leonardo Giordani: https://github.com/lgiordani/cookiecutter-pypackage



Gitlab
``````
Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor
de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello
publicado bajo una Licencia de código abierto. En el proyecto lo emplearemos para alojar nuestro codigo y armar un
pipeline para CI/CD.

Para este paso se requiere tener una cuenta de gitlab, con la cual nos debemos loguear y posteriormente lo que haremos
es crear un grupo publico (en este caso yo lo nombre wired), que el mismo nos permitira administrar todo el proyecto.

Dentro de este grupo se crearan los repositorios publicos que se ven en la imagen.

.. figure:: repos.png
    :align: center
    :width: 100%

    Repositorios de gitlab


Ahora se debe sincronizar el proyecto que creamos anteriormente con el repositorio del backend. Para debemos navegar a
la raiz del proyecto (lain_backend) y ejecutar los siguientes comandos:

.. code-block:: console

    $ git init
    $ git remote add origin git@gitlab.com:himuraxkenji/lain_backend.git
    $ git add .
    $ git commit -m "feat: initial commit"
    $ git push -u origin master

Con esto ya se encontraria sincronizado el repositorio en la nube con el local.

Flake8
```````
Los linter son herramientas que permiten realizar un analisis estatico del codigo fuente para verificar si hay
discrepancias sintacticas. Los linters comprobaran la sintaxis del código e indicaran donde se debe corregir.

Existen diferentes linters para python, pero para este proyecto emplearemos Flake8, el cual permite verificar
el estandar de codificacion PEP-8, y la complejidad circular.

Para realizar la instalacion se debe ejecutar el siguiente comando:

.. code-block:: console

    $ pip install flake8

Una vez instalado, deberemos crear el script en el Makefile para poder ejecutar el linter. Dentro de la carpeta raiz
del proyecto abrimos el archivo Makefile y debemos agregar la siguiente linea de codigo:

.. code-block:: makefile

    lint:
	    flake8 lain_backend tests


Con eso ya podremos ejecutar el linter desde la raiz del proyecto con :code:`make lint` y podremos ver los errores de
sintaxis del codigo fuente y los test.

.. role:: bash(code)
   :language: bash

Black
```````
Black es un formateador de código Python, que nos permitira poder ahorrar tiempo a la hora de corregir los errores
reportados por el linter. Para instalarlo debemos ejecutar:

.. code-block:: console

    $ pip install black

Adicionalmente debemos configurar flake8, ya que existen 2 codigo que black no respeta del linter. Se debe abrir el
archivo .flake8 que se encuentra en la raiz del proyecto y agregar el siguiente bloque de codigo:

.. code-block:: console

    [flake8]
    max-line-length = 88
    extend-ignore = E203, W503

Como ultimo paso, ahora debemos añadir al archivo Makefile el script de para ejecutar el formatter:

.. code-block:: makefile

    format:
        black lain_backend tests

Ahora con esto ya podremos formatear nuestro codigo para que respete el estandar usando :code:`make format`.

.. role:: bash(code)
   :language: bash
