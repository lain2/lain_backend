Frontend
========

El frontend constituye la presentacion de la aplicacion. Debido a 
que se decidio que el backend sea una API/REST, podremos crear 
este proyecto de forma aparte. Para poder conectar este proyecto
emplearemos peticiones HTTP mediante Javascript.

Se eligio para la construccion, del frontend la libreria React 17,
la cual permite construir interfaces empleando el enfoque de componentes,
lo cual permite reutilizar los mismo para el proyecto.

En el proyecto se emplearan test unitarios y de integridad, analisis estatico,
y se versionado con CI/CD que valide que el codigo es correcto y no se
pueda subir a produccion codigo que no pase los estandares de calidad establecido
para el mismo. Como punto adicional se empleara la convencional commmit
para realizar los commits del repositorio.

Contenido

.. toctree::
    :maxdepth: 1
    :includehidden:

    herramientas
    ci-cd
