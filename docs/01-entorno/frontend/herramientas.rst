Herramientas
============


Para poder crear el entorno de trabajo, debemos tener instalado NodeJS, NPM y NPX.
Mediante del entrono de ejecucion para JS y el manejador de paquetes realizaremos
la creacion del entorno. 

En esta seccion se creara el proyecto y se agregaran herramientas de 
automatizacion de calidad de codigo, las cuales serviran para automatizar
mucho trabajo.

.. note::
   Para este trabajo se empleo NodeJS 12.19.0, NPM 6.14.8 y NPX 6.14.8.


React
`````

Sera la libreria con la cual construiremos las interfaces del proyecto y sobre la cual se
basara toda forma de trabajo del frontend.

Para realizar la instalacion de create-react-app, el cual nos permitira crear un
proyecto de react desde cero. Para instalarlo emplearemos el comando:

.. code-block:: console

    $ npm install -g create-react-app

Una vez realizada la instalacion debemos ejecutar el siguiente comando para 
crear el proyecto:

.. code-block:: console

    $ npx create-react-app lain_frontend

Donde **lain_frontend** es el nombre del proyecto.

Conventional Commits y git cz
`````````````````````````````
Es un sistema para estandarizar el formato de los commits, no es una herramienta 
como tal, pero permite grandes ventajas si se respeta este sistema, como es
la automatizacion de determinadas tareas, ejemplo de esto puede ser la 
generacion de CHANGELOGS.

El formato que deben seguir los commits debe ser el siguiente

.. code-block:: 

    <type>[optional scope]: <description>
    [optional body]
    [optional footer(s)]

Para poder realizar los commits en este formato y que no se realicen de otra
forma, emplearemos commitizen, el cual es un paquete de node que nos permite
mediante un comando ver el tipo de commit que queremos realizar, dando una
idea de que trata cada uno y a que apunta el mismo. 

La instalacion de commitizen la realizamos mediante el comando:

.. code-block:: console

    $ npm install -g commitizen

y posteriomente lo que hacemos cada vez que agregamos un cambio al stage
area del repositorio, ejecutamos el comando:

.. code-block:: console
    
    $ git cz

y como resultado tendremos el siguiente wizzard, con el cual haremos los commits.

.. figure:: add-commit.png
    :align: center
    :width: 100%

    Wizzard de commitizen

Prettier y ESLint
`````````````````
Pretier es una herramienta que sirve formatear codigo. Lo que hace es 
analizar el codigo y luego aplica un formato que configuremos.

EsLint es sencillamente una herramienta de linting para JavaScript y JSX
cuya función es la de revisar nuestro código tratando de encontrar errores 
que podrían provocar problemas de compilación o bien futuros bugs en nuestro 
desarrollo. Cuando EsLint encuentra algún error, o bien lo repara de forma
automática o bien nos advierte de ello a través de distintos mensajes, 
basándose para ello en un conjunto de reglas definidas previamente y que 
son altamente configurables.

Para instalar Prettier y ESLint debemos correr en la terminal los siguientes 
comandos:

.. code:: console
    
   $ npm install eslint eslint-config-airbnb --save-dev
   $ npm install prettier --save-dev

.. note::

    Para este proyecto se empleo la configuracion de Airbnb mas algunas reglas
    propias

Ahora debemos configurar eslint para que detecte errores funcionales, para 
esto creamos un archivo en la raiz del proyecto llamado .eslintrc.json

.. code:: json
   
    {
      "plugins": ["react", "prettier"],
      "extends": ["airbnb", "prettier", "plugin:react/recommended"],
      "env": {
        "es2020": true,
        "browser": true,
        "jest": true
      },
      "parserOptions": {
        "sourceType": "module",
        "ecmaVersion": 2020,
        "ecmaFeatures": {
          "jsx": true
        }
      },
      "rules": {
        "no-underscore-dangle": "off",
        "arrow-body-style": "off",
        "no-shadow": "off",
        "consistent-return": "off",
        "no-nested-ternary": "off",
        "no-console": "off",
        "no-case-declarations": "off",
        "import/prefer-default-export": "off",
        "prettier/prettier": "error",
        "no-new-object": "off",
        "react/jsx-props-no-spreading": "off",
        "react/prop-types": 0,
        "react/jsx-curly-newline": "off"
      }
    }

Tambien se creara un archivo llamado .eslintignore, donde especificaremos que 
archivos no aplicara realizara ningun analisis eslint.  El archivo debe contener
el siguiente codigo

.. code:: json

    *.css
    *.scss
    *.rst

Husky
`````
Es un configurador de hooks de git, que cuando se ejecuta una accion, el 
detecta que se ejecuto dicha accion y poder realizar una accion previa a 
la accion base. En el proyecto se empleara para poder ejecutar prettier y
ESLint antes de poder realizar un commit y de esta forma no dejar que se 
suba al repositorio codigo que no se encuentre formateado y con todos
los errores de formato corregidos.

Para instalar husky debemos ejecutar:

.. code-block:: console

    $ npm install husky --save-dev


Una vez instalado debemos crear la accion que se ejecutara cada vez 
que realicemos un commit (pre-commit). Para ello iremos al archivo 
package.json en la raiz del proyecto de react y agregaremos el siguiente
fragmento de codigo al final.


.. code-block:: json

  "husky": {
    "hooks": {
      "pre-commit": "npx prettier --write src/ && npx eslint --fix \"src/**\" "
    }
  }


Con esto cada vez que se quiera subir codigo "sucio" al repositorio, husky
aplicara prettier y eslint y si estos generan algun error, entonces 
solicitara que se corrija el error para poder subir dicho codigo.

