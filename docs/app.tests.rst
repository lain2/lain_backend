app.tests package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   app.tests.core
   app.tests.data
   app.tests.rest

Module contents
---------------

.. automodule:: app.tests
   :members:
   :undoc-members:
   :show-inheritance:
