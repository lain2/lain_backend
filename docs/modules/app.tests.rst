app.tests package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   app.tests.core
   app.tests.data

Module contents
---------------

.. automodule:: app.tests
   :members:
   :undoc-members:
   :show-inheritance:
