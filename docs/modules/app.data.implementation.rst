app.data.implementation package
===============================

Submodules
----------

app.data.implementation.create\_study\_plan module
--------------------------------------------------

.. automodule:: app.data.implementation.create_study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.data.implementation
   :members:
   :undoc-members:
   :show-inheritance:
