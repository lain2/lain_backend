app.core.models package
=======================

Submodules
----------

app.core.models.date\_audit module
----------------------------------

.. automodule:: app.core.models.date_audit
   :members:
   :undoc-members:
   :show-inheritance:

app.core.models.study\_plan module
----------------------------------

.. automodule:: app.core.models.study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.core.models
   :members:
   :undoc-members:
   :show-inheritance:
