app.data.crud package
=====================

Submodules
----------

app.data.crud.base module
-------------------------

.. automodule:: app.data.crud.base
   :members:
   :undoc-members:
   :show-inheritance:

app.data.crud.create\_study\_plan module
----------------------------------------

.. automodule:: app.data.crud.create_study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.data.crud
   :members:
   :undoc-members:
   :show-inheritance:
