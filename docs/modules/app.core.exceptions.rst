app.core.exceptions package
===========================

Submodules
----------

app.core.exceptions.study\_plan\_exception module
-------------------------------------------------

.. automodule:: app.core.exceptions.study_plan_exception
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.core.exceptions
   :members:
   :undoc-members:
   :show-inheritance:
