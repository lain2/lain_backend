app.tests.data.crud package
===========================

Submodules
----------

app.tests.data.crud.test\_create\_study\_plan module
----------------------------------------------------

.. automodule:: app.tests.data.crud.test_create_study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.tests.data.crud
   :members:
   :undoc-members:
   :show-inheritance:
