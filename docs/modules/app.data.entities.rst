app.data.entities package
=========================

Submodules
----------

app.data.entities.study\_plan\_entity module
--------------------------------------------

.. automodule:: app.data.entities.study_plan_entity
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.data.entities
   :members:
   :undoc-members:
   :show-inheritance:
