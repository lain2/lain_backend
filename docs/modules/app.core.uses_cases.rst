app.core.uses\_cases package
============================

Submodules
----------

app.core.uses\_cases.create\_study\_plan\_uc module
---------------------------------------------------

.. automodule:: app.core.uses_cases.create_study_plan_uc
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.core.uses_cases
   :members:
   :undoc-members:
   :show-inheritance:
