app.tests.core.models package
=============================

Submodules
----------

app.tests.core.models.test\_study\_plan module
----------------------------------------------

.. automodule:: app.tests.core.models.test_study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.tests.core.models
   :members:
   :undoc-members:
   :show-inheritance:
