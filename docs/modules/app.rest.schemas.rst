app.rest.schemas package
========================

Submodules
----------

app.rest.schemas.study\_plan module
-----------------------------------

.. automodule:: app.rest.schemas.study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.rest.schemas
   :members:
   :undoc-members:
   :show-inheritance:
