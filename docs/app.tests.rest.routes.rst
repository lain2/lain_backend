app.tests.rest.routes package
=============================

Submodules
----------

app.tests.rest.routes.conftest module
-------------------------------------

.. automodule:: app.tests.rest.routes.conftest
   :members:
   :undoc-members:
   :show-inheritance:

app.tests.rest.routes.test\_create\_study\_plan module
------------------------------------------------------

.. automodule:: app.tests.rest.routes.test_create_study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.tests.rest.routes
   :members:
   :undoc-members:
   :show-inheritance:
