app.data package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   app.data.crud
   app.data.db
   app.data.entities
   app.data.implementation

Submodules
----------

app.data.deps module
--------------------

.. automodule:: app.data.deps
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.data
   :members:
   :undoc-members:
   :show-inheritance:
