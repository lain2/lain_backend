app.rest.routes package
=======================

Submodules
----------

app.rest.routes.create\_study\_plan module
------------------------------------------

.. automodule:: app.rest.routes.create_study_plan
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.rest.routes
   :members:
   :undoc-members:
   :show-inheritance:
