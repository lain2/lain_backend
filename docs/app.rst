app package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   app.config
   app.core
   app.data
   app.rest
   app.tests

Submodules
----------

app.asgi module
---------------

.. automodule:: app.asgi
   :members:
   :undoc-members:
   :show-inheritance:

app.config module
-----------------

.. automodule:: app.config
   :members:
   :undoc-members:
   :show-inheritance:

app.main module
---------------

.. automodule:: app.main
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app
   :members:
   :undoc-members:
   :show-inheritance:
