.. Lain documentation master file, created by
   sphinx-quickstart on Tue Jan  5 23:23:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lain's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

    01-entorno/configuracion
    modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
