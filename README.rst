===============================
lain_backend
===============================


.. image:: https://img.shields.io/pypi/v/lain_backend.svg
        :target: https://pypi.python.org/pypi/lain_backend

.. image:: https://img.shields.io/travis/himuraxkenji/lain_backend.svg
        :target: https://travis-ci.org/himuraxkenji/lain_backend

.. image:: https://readthedocs.org/projects/lain-backend/badge/?version=latest
        :target: https://lain-backend.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/himuraxkenji/lain_backend/shield.svg
     :target: https://pyup.io/repos/github/himuraxkenji/lain_backend/
     :alt: Updates


Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://lain-backend.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

