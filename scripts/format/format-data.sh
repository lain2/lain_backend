#!/usr/bin/env bash

# poetry run mypy app/core/*
echo "============DATA==================="
poetry run autoflake --in-place --remove-unused-variables --remove-all-unused-imports app/data/*/*.py
poetry run black app/data/*
poetry run isort app/data/*/*.py
echo "==================================="

