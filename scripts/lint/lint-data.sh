#!/usr/bin/env bash

echo "============DATA==================="
poetry run mypy app/data
poetry run black app/data/* --check
poetry run isort app/data/* --check
poetry run flakehell lint app/data/*
echo "==================================="
