#!/usr/bin/env bash

echo "============REST==================="
poetry run mypy app/rest/*
poetry run black app/rest/* --check
poetry run isort app/rest/* --check
poetry run flakehell lint app/rest/*
echo "==================================="
