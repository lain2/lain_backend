#!/usr/bin/env bash

echo "============CORE==================="
poetry run mypy app/core
poetry run black app/core/* --check
poetry run isort app/core/* --check
poetry run flakehell lint app/core/*
echo "==================================="
